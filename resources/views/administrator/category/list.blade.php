@extends('administrator.layouts.master')
@section('title', 'Danh sách chuyên mục '.getTypeName($type))

@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">{{getTypeName($type)}}</span>
            <h3 class="page-title">Danh sách danh mục {{getTypeName($type)}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <form class="row">
                        <div class="col">
                            <input class="form-control" placeholder="Tìm theo tên" name="name" value="{{$request->name}}"/>
                        </div>
                        <div class="col">
                            <select class="form-control" name="parent">
                                <option value="0">Tìm theo mục cha</option>
                                @foreach($cates as $category)
                                    <option value="{{$category->cat_id}}" {{($request->parent == $category->cat_id)? 'selected': ''}}>{{$category->cat_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <button type="submit" class="mb-2 btn btn-sm btn-success mr-1">Lọc danh mục</button>
                        </div>
                        <div class="col">
                            <button onclick="themDanhmuc()" type="button" class="mb-2 btn btn-sm btn-primary mr-1">Thêm danh mục</button>
                        </div>
                    </form>
                </div>
                <div class="card-body p-0 pb-3">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Tên chuyên mục</th>
                                <th scope="col" class="border-0">Đường dẫn</th>
                                <th scope="col" class="border-0">Mục cha</th>
                                <th scope="col" class="border-0">Thứ tự</th>
                                <th scope="col" class="border-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            @if($category->cat_name != 'uncategorized')
                            <tr>
                                <td>{{$category->cat_id}}</td>
                                <td><input type="hidden" id="row_name_{{$category->cat_id}}" value="{{$category->cat_name}}"/>
                                    <input type="hidden" id="row_image_{{$category->cat_id}}" value="{{$category->cat_image}}"/>
                                    {{$category->cat_name}}</td>
                                <td><input type="hidden" id="row_alias_{{$category->cat_id}}" value="{{($type == 'post') ? $category->alias->alias : $category->$type->alias->alias}}"/>{{($type == 'post') ? $category->alias->alias : $category->$type->alias->alias}}</td>
                                <td><input type="hidden" id="row_parent_{{$category->cat_id}}" value="{{$category->cat_parent}}"/>{{($category->cat_parent>0)?$category->parent->cat_name: '--'}}</td>
                                <td><input class="form-control" id="row_order_{{$category->cat_id}}" value="{{$category->cat_order}}" onchange="changeOrder({{$category->cat_id}})"></td>
                                <td>
                                    <button class="mb-2 btn btn-sm btn-primary mr-1" onclick="themDanhmuc({{$category->cat_id}})"><i class="material-icons">add</i> Thêm mục con</button>
                                    <button class="mb-2 btn btn-sm btn-warning mr-1" onclick="chinhsuaDanhmuc({{$category->cat_id}})"><i class="material-icons">edit</i> Chỉnh sửa</button>
                                    <button class="mb-2 btn btn-sm btn-danger mr-1" onclick="deleteCat({{$category->cat_id}})"><i class="material-icons">delete</i> Xóa</button>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                        {!! $categories->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <form class="modal" tabindex="-1" role="dialog" id="updateModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span id="form_name">Thêm</span> danh mục</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="form_action" value="add">
                    <input type="hidden" id="input_id" name="id">
                    <div class="form-group">
                        <input type="text" id="input_name" name="name" class="form-control" placeholder="Tên danh mục" onfocus="removeInValid('name')">
                        <div id="error_name" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" id="input_alias" name="alias" class="form-control" placeholder="Đường dẫn" onfocus="removeInValid('alias')">
                        <div id="error_alias" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <input id="uploadInput" name="image"/>
                    </div>
                    <div class="form-group">
                        <select type="text" id="input_parent" name="parent" class="form-control">
                            <option value="0">Danh mục gốc</option>
                            @foreach($cates as $category)
                                <option value="{{$category->cat_id}}">{{$category->cat_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="submitForm()" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('scripts')
    <link rel="stylesheet" href="/plugins/ktupload/css/ktupload.min.css"/>
    <script src="/plugins/ktupload/js/ktupload-min.js"></script>
<script>

    $('#uploadInput').KTUInputUpload({
        curent_item: "https://www.abc.net.au/news/image/9670912-3x2-700x467.jpg",
        upload_url: '{{route('admin-upload-input')}}?_token={{csrf_token()}}',
        loading_icon : '/plugins/ktupload/img/loadingsvg.svg'
    })

    function themDanhmuc(i = null){
        $('#uploadInput').val();
        $('#ktu-thumb-uploadInput').attr('src','');

        $('#updateModal').modal('show')
        $('#form_action').val('add')
        $('#input_id').val('')
        $('#input_name').val('')
        $('#input_parent').val(0)
        $('#input_alias').val('')
    }
    function chinhsuaDanhmuc(i = null){
        $('#uploadInput').val($('#row_image_'+i).val());
        $('#ktu-thumb-uploadInput').attr('src',getImage($('#row_image_'+i).val()));

        $('#updateModal').modal('show')
        $('#form_action').val('edit')
        $('#input_id').val(i)
        $('#input_name').val($('#row_name_'+i).val())
        $('#input_parent').val($('#row_parent_'+i).val())
        $('#input_alias').val($('#row_alias_'+i).val())
    }
    function submitForm(){
        var action = $('#form_action').val();
        if(action == 'add'){
            addCat();
        }else{
            editCat();
        }
    }
    function editCat(){
        var formData = $('#updateModal').serialize()
        $.ajax({
            url: '{{route('admin-post-cat-edit',['type'=>$type])}}',
            data: formData,
            type: 'post'
        }).then(function (res) {
            $.toast({
                heading: 'Thao tác thành công',
                text: res.message,
                hideAfter: 500,
                icon: 'success',
                afterHidden: function afterHidden() {
                    window.location.reload();
                }
            });
        }).catch(function (e) {
            console.log(e); //alert(e.responseText)

            $.toast({
                heading: 'Lỗi',
                text: e.responseJSON.message,
                showHideTransition: 'fade',
                icon: 'error',
                hideAfter: false
            });
            console.log(e.responseJSON.errors)
            $.each(e.responseJSON.errors, function(k, item){
                console.log(k, item)
                $('#error_'+k).text(item[0])
                $('#input_'+k).addClass('is-invalid')
            })
        });
    }
    function addCat(){
        var formData = $('#updateModal').serialize()
        $.ajax({
            url: '{{route('admin-post-cat-add',['type'=>$type])}}',
            data: formData,
            type: 'post'
        }).then(function (res) {
            $.toast({
                heading: 'Thao tác thành công',
                text: res.message,
                hideAfter: 500,
                icon: 'success',
                afterHidden: function afterHidden() {
                    window.location.reload();
                }
            });
        }).catch(function (e) {
            console.log(e); //alert(e.responseText)

            $.toast({
                heading: 'Lỗi',
                text: e.responseJSON.message,
                showHideTransition: 'fade',
                icon: 'error',
                hideAfter: false
            });
            console.log(e.responseJSON.errors)
            $.each(e.responseJSON.errors, function(k, item){
                console.log(k, item)
                $('#error_'+k).text(item[0])
                $('#input_'+k).addClass('is-invalid')
            })
        });
    }

    function deleteCat(i){
        var confirmed = confirm('Bạn có chắc chắn muốn xóa đối tượng này?')
        if(confirmed){
            $.ajax({
                url: '{{route('admin-post-cat-delete',['type'=>$type])}}',
                data: {_token: '{{csrf_token()}}', id: i},
                type: 'post'
            }).then(function (res) {
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.reload();
                    }
                });
            }).catch(function (e) {
                console.log(e); //alert(e.responseText)

                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message,
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            });
        }

    }

    function changeOrder(i){
        var order = $('#row_order_'+i).val()
        $.ajax({
            url: '{{route('admin-post-cat-change-order',['type'=>$type])}}',
            data: {_token: '{{csrf_token()}}', id: i, order: order},
            type: 'post'
        }).then(function (res) {
            $.toast({
                heading: 'Thao tác thành công',
                text: res.message,
                hideAfter: 500,
                icon: 'success',
                afterHidden: function afterHidden() {
                    //window.location.reload();
                }
            });
        }).catch(function (e) {
            console.log(e); //alert(e.responseText)

            $.toast({
                heading: 'Lỗi',
                text: e.responseJSON.message,
                showHideTransition: 'fade',
                icon: 'error',
                hideAfter: false
            });
        });
    }

    function removeInValid(id){
        $('#input_'+id).removeClass('is-invalid')
    }
</script>
@endsection