<!-- Main Sidebar -->
<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="/admin/images/logo.svg" alt="CMSDashboard">
                    <span class="d-none d-md-inline ml-1">CMS Dashboard</span>
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>

    <div class="nav-wrapper">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='')? 'active':''}}" href="{{route('admin-home')}}">
                    <i class="material-icons">timeline</i>
                    <span>Tổng quan</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='category' && Request::segment(3)=='post')? 'active':''}}" href="{{route('admin-post-cat',['type'=>'post'])}}">
                    <i class="material-icons">vertical_split</i>
                    <span>Danh mục bài viết</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='post' && Request::segment(3)=='post')? 'active':''}}" href="{{route('admin-post-list',['type'=>'post'])}}">
                    <i class="material-icons">note_add</i>
                    <span>Danh sách bài viết</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='category' && Request::segment(3)=='project')? 'active':''}}" href="{{route('admin-post-cat',['type'=>'project'])}}">
                    <i class="material-icons">vertical_split</i>
                    <span>Danh mục Dự án</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='post' && Request::segment(3)=='project')? 'active':''}}" href="{{route('admin-post-list',['type'=>'project'])}}">
                    <i class="material-icons">note_add</i>
                    <span>Danh sách Dự án</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='post' && Request::segment(3)=='page')? 'active':''}}" href="{{route('admin-post-list',['type'=>'page'])}}">
                    <i class="material-icons">vertical_split</i>
                    <span>Quản lý trang</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='post' && Request::segment(3)=='block')? 'active':''}}" href="{{route('admin-post-list',['type'=>'block'])}}">
                    <i class="material-icons">note_add</i>
                    <span>Quản lý khối nội dung</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(Request::segment(2)=='setting')? 'active':''}}" href="{{route('admin-setting')}}">
                    <i class="material-icons">settings</i>
                    <span>Cấu hình thông tin</span>
                </a>
            </li>
        </ul>
    </div>
</aside>