@extends('administrator.layouts.master')
@section('title', 'Cấu hình trang')

@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Cấu hình</span>
            <h3 class="page-title">Cá nhân</h3>
        </div>
    </div>
    <form id="settingForm" class="row" method="post" action="{{route('admin-change-pass-process')}}">
        @csrf
        <div class="col-sm-12">
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Đổi mật khẩu</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Mật khẩu cũ</div>
                        <div class="col-9"><input class="form-control" name="oldpass" type="password"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Mật khẩu mới</div>
                        <div class="col-9"><input class="form-control" name="newpass" type="password"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Xác nhận</div>
                        <div class="col-9"><input class="form-control" name="cnewpass" type="password"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3"></div>
                        <div class="col-9"><button class="btn btn-success">Thay đổi</button></div>
                    </div>
                </div>
            </div>
        </div>

    </form>

@stop
@section('style')
    <link rel="stylesheet" href="/plugins/ktupload/css/ktupload.min.css"/>
@endsection
@section('scripts')
    <script>
    $('#settingForm').submit(function(e){
        e.preventDefault()
        var formData = $(this).serialize()
        $.ajax({
            url: "{{route('admin-change-pass-process')}}",
            data: formData,
            type: "POST",
        }).then(function(res){
            alert(res)
        }).catch(function(e){
            alert(e.responseText)
        })
        return false;
    })
    </script>
@endsection