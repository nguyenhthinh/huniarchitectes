@extends('administrator.layouts.master')
@section('title', 'Thêm '.getTypeName($type))

@section('content')

<!-- / .main-navbar -->
<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">{{getTypeName($type)}}</span>
            <h3 class="page-title">Thêm {{getTypeName($type)}}</h3>
        </div>
    </div>
    <!-- End Page Header -->
    <form id="postForm" class="row" method="post" action="{{route('admin-post-add-process',['type'=>'post'])}}">
        @csrf
        <div class="col-lg-9 col-md-12">
            
            <!-- Add New Post Form -->
            <div class="card card-small mb-3">
                <div class="card-body">
                    <div class="add-new-post">
                        <input class="form-control form-control-lg mb-3" name="name" id="title" type="text" onkeyup="change_alias('#title', '#alias')" placeholder="Tiêu đề {{getTypeName($type)}}">
                        <input class="form-control form-control-lg mb-3"  name="alias" id="alias" type="text" placeholder="Đường dẫn {{getTypeName($type)}}">
                        @if($type != 'project')
                        <textarea name="description" placeholder="Tóm tắt" class="form-control mb-3"></textarea><br/>
                        <textarea name="content" class=" class="form-control mb-3"></textarea><br/>
                        @endif
                        <input class="form-control form-control-lg mb-3" name="meta_title" placeholder="SEO Meta title"/>
                        <input class="form-control form-control-lg mb-3" name="meta_description" placeholder="SEO Meta description"/>
                    </div>
                </div>
            </div>
            @if($type == 'project')
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Gallery</h6>
                    </div>
                    <div class='card-body p-0'>
                        <div class="row-fluid border-0">
                            <div class="col-sm-12">

                                <div id="uploadSection" class="pt-3">
                                    <div class="ktu-wrapper" id="gallerySection">
                                            <div class="ktu-item" id="ktu-add-item">
                                                    <span class="ktu-select-btn" onclick="KTUAddItem()">ADD IMAGE</span>
                                                    <div class="ktu-thumb"><img src="/plugins/ktupload/img/add-photo.png"></div>
                                                    <input type="hidden" value="0" id="galCount"/>
                                                    <input type="hidden" value="" id="setimage"/>
                                            </div>
                                    </div>
                                    
                                </div>
                                <br/>
                                <input class="form-control form-control-lg mb-3" name="project_video" placeholder="Video"/>
                                <input class="form-control form-control-lg mb-3" name="project_year" placeholder="Year"/>
                                <input class="form-control form-control-lg mb-3" name="project_location" placeholder="Location"/>
                                <input class="form-control form-control-lg mb-3" name="project_area" placeholder="Area"/>
                                <input class="form-control form-control-lg mb-3" name="project_cost" placeholder="Cost"/>
                                <input class="form-control form-control-lg mb-3" name="project_designer" placeholder="Designer"/>
                                <input class="form-control form-control-lg mb-3" name="project_description" placeholder="Description"/>
                            </div>
                        </div>
                    </div>
                </div>
        @endif
            <!-- / Add New Post Form -->
        </div>
        <div class="col-lg-3 col-md-12">

            <!-- Post Overview -->
            <div class='card card-small mb-3'>
                <div class="card-header border-bottom">
                    <h6 class="m-0">Cơ bản</h6>
                </div>
                <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item">
                            <label>Hình ảnh</label>
                            <div class="ktu-input-wrapper" id="featureSection">
                                <div class="ktu-item" id="ktu-item-feature">
                                        <span class="ktu-select-btn" onclick="KTUChooseImage('feature')" id="ktu-add-btn-feature">CHOOSE IMAGE</span>
                                        <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-feature" src="/plugins/ktupload/img/default-image.png"></div>
                                        <input type="hidden" name="image" value="" id="ktu-image-feature">
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <select class="form-control" name="parent">
                                @foreach($cates as $category)
                                    <option value="{{$category->cat_id}}" {{($request->parent == $category->cat_id)? 'selected': ''}}>{{$category->cat_name}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="list-group-item d-flex px-3">
                            {{--<button class="btn btn-sm btn-outline-accent">
                                <i class="material-icons">save</i> Save Draft</button>--}}
                            <button class="btn btn-sm btn-accent ml-auto" type="button" onclick="savePost()">
                                <i class="material-icons">file_copy</i> Publish</button>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- / Post Overview -->
            <!-- Post Overview -->

            <!-- / Post Overview -->
        </div>
    </form>
</div>


@stop

@section('style')
    <link rel="stylesheet" href="/plugins/ktupload/css/ktupload.min.css"/>
    <style>
        .ktu-wrapper .ktu-item .ktu-remove-btn {
            position: absolute;
            top: -5px;
            right: -5px;
            padding: 10px;
            background: red;
            color: #FFF;
            font-weight: bold;
            cursor: pointer;
            border-radius: 50%;
            height: 20px;
            width: 20px;
            text-align: center;
            z-index: 3;
            line-height: 0px;
            text-align: center;
        }
    </style>
@endsection

@section('scripts')
    <script src="/plugins/ckfinder/ckfinder.js"></script>
    <script src="/plugins/ktupload/js/ktupload2.js"></script>
    <script>
        function change_alias(title, alias) {
            var str = $(title).val();
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str = str.replace(/đ/g,"d");
            str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,"");
            str = str.replace(/ + /g,"-");
            str = str.replace(/ /g,"-");
            str = str.trim();
            $(alias).val(str)
        }
    </script>
    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script>
@if($type != 'project')
        CKEDITOR.editor.fileUploadRequest = function( evt ) {
            var fileLoader = evt.data.fileLoader,
                formData = new FormData(),
                xhr = fileLoader.xhr;

            xhr.open( 'PUT', fileLoader.uploadUrl, true );
            xhr.setRequestHeader('X-CSRF-TOKEN', '{{csrf_token()}}');
            formData.append( 'upload', fileLoader.file, fileLoader.fileName );
            fileLoader.xhr.send( formData );

            // Prevented the default behavior.
            evt.stop();
        }
        CKEDITOR.replace( 'content', {
            uploadUrl: '{{route('admin-upload')}}?_token={{csrf_token()}}',
            filebrowserBrowseUrl: '{{route('admin-upload-browser')}}?_token={{csrf_token()}}',
            filebrowserUploadUrl: '{{route('admin-upload')}}?_token={{csrf_token()}}'
        } );
        @endif

        @if($type == 'project')
        var gl_photo_item = `<div class="ktu-item" id="ktu-item-{ID}">
                                <span class="ktu-select-btn" onclick="KTUChooseImage({ID})" id="ktu-add-btn-{ID}">CHOOSE IMAGE</span>
                                <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-{ID}" src="/plugins/ktupload/img/default-image.png"></div>
                                <span class="ktu-remove-btn ktu-show" id="ktu-remove-btn-{ID}" onclick="KTUDeleteItem({ID})">x</span>
                                <input type="hidden" name="project_gallery[]" value="" id="ktu-image-{ID}">
                            </div>`;
        function KTUAddItem(){
            var i = $('#galCount').val();
            i++;
            var addItem = gl_photo_item;
            addItem = addItem.replace(/{ID}/g, i);
            console.log(addItem);
            $('#gallerySection').append(addItem);
            $('#galCount').val(i);
        }
        @endif

        function savePost(){
            //$('#postForm').submit()
            @if($type != 'project')
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            @endif
            var formData = $('#postForm').serialize()
            $.ajax({
                url: '{{route('admin-post-add-process',['type'=>$type])}}',
                data: formData,
                type: 'post'
            }).then(function (res) {
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.href = '{{route('admin-post-list',['type'=>$type])}}';
                    }
                });
            }).catch(function (e) {
                console.log(e); //alert(e.responseText)
                var html = ''
                $.each(e.responseJSON.errors, function(k, item){
                    console.log(k, item)
                    html+= '<li>'+k+': '+item[0]+'</li>';
                })
                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message + '<ul>'+html+'</ul>',
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            });
        }
    </script>

@endsection