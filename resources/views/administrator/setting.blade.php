@extends('administrator.layouts.master')
@section('title', 'Cấu hình trang')

@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Cấu hình</span>
            <h3 class="page-title">Cấu hình trang</h3>
        </div>
    </div>
    <form id="settingForm" class="row">
        @csrf
        <div class="col-sm-6">
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Thông tin chung</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Tên trang</div>
                        <div class="col-9"><input class="form-control" name="option[site][name]" value="{{getOption('site','name')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">META_title</div>
                        <div class="col-9"><input class="form-control" name="option[site][meta_title]" value="{{getOption('site','meta_title')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">META_description</div>
                        <div class="col-9"><input class="form-control" name="option[site][meta_description]" value="{{getOption('site','meta_description')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Từ khóa</div>
                        <div class="col-9"><input class="form-control" name="option[site][keywords]" value="{{getOption('site','keywords')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-2">Logo</div>
                        <div class="col-sm-4">
                            <!-- <input id="uploadLogo" name="option[site][logo]"  value="{{getOption('site','logo')}}"/> -->
                            <div class="ktu-input-wrapper" id="featureSection">
                                <div class="ktu-item" id="ktu-item-logo_site">
                                        <span class="ktu-select-btn" onclick="KTUChooseImage('logo_site')" id="ktu-add-btn-logo_site">CHOOSE IMAGE</span>
                                        <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-logo_site" src="{{getOption('site','logo')}}"></div>
                                        <input type="hidden" name="option[site][logo]" value="{{getOption('site','logo')}}" id="ktu-image-logo_site">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">Favicon</div>
                        <div class="col-sm-4">
                            <!-- <input id="uploadFavicon" name="option[site][favicon]" value="{{getOption('site','favicon')}}"/> -->
                            <div class="ktu-input-wrapper" id="featureSection">
                                <div class="ktu-item" id="ktu-item-favicon">
                                        <span class="ktu-select-btn" onclick="KTUChooseImage('favicon')" id="ktu-add-btn-favicon">CHOOSE IMAGE</span>
                                        <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-favicon" src="{{getOption('site','favicon')}}"></div>
                                        <input type="hidden" name="option[site][favicon]" value="{{getOption('site','favicon')}}" id="ktu-image-favicon">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Thông tin chủ sở hữu</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Công ty</div>
                        <div class="col-9"><input class="form-control" name="option[author][name]" value="{{getOption('author','name')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Địa chỉ</div>
                        <div class="col-9"><input class="form-control" name="option[author][address]" value="{{getOption('author','address')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Số điện thoại</div>
                        <div class="col-9"><input class="form-control" name="option[author][phone]" value="{{getOption('author','phone')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Email</div>
                        <div class="col-9"><input class="form-control" name="option[author][email]" value="{{getOption('author','email')}}"/></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Mạng xã hội</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Facebook</div>
                        <div class="col-9"><input class="form-control" name="option[social][facebook]" value="{{getOption('social','facebook')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Twitter</div>
                        <div class="col-9"><input class="form-control" name="option[social][twitter]" value="{{getOption('social','twitter')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Youtube</div>
                        <div class="col-9"><input class="form-control" name="option[social][youtube]" value="{{getOption('social','youtube')}}"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Instagram</div>
                        <div class="col-9"><input class="form-control" name="option[social][instagram]" value="{{getOption('social','instagram')}}"/></div>
                    </div>
                </div>
            </div>
            <div class="card card-small mb-4">

                <div class="card-body p-3">
                    <div class="row">
                        <div class="col">
                            <button onclick="saveSetting()" type="button" class="btn btn-primary btn-block">Lưu lại</button>
                        </div>
                        {{--<div class="col">
                            <button type="button" class="btn btn-warning btn-block">Xóa Cache</button>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </form>
<input type="hidden" id="setimage"/>
@stop
@section('style')
    <link rel="stylesheet" href="/plugins/ktupload/css/ktupload.min.css"/>
@endsection
@section('scripts')
    <script src="/plugins/ckfinder/ckfinder.js"></script>
<script src="/plugins/ktupload/js/ktupload2.js"></script>
    <script>
        // $('#uploadLogo').KTUInputUpload({
        //     curent_item: "{{showThumb(getOption('site','logo'))}}",
        //     upload_url: '{{route('admin-upload-input')}}?_token={{csrf_token()}}',
        //     loading_icon : '/plugins/ktupload/img/loadingsvg.svg'
        // })
        // $('#uploadFavicon').KTUInputUpload({
        //     curent_item: "{{showThumb(getOption('site','favicon'))}}",
        //     upload_url: '{{route('admin-upload-input')}}?_token={{csrf_token()}}',
        //     loading_icon : '/plugins/ktupload/img/loadingsvg.svg'
        // })

        function saveSetting(){
            var formData = $('#settingForm').serialize()
            $.ajax({
                url: '{{route('admin-setting-save')}}',
                data: formData,
                type: 'post'
            }).then(function(res){
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.reload();
                    }
                });
            }).catch(function(e){
                var html = ''
                $.each(e.responseJSON.errors, function(k, item){
                    console.log(k, item)
                    html+= '<li>'+k+': '+item[0]+'</li>';
                })
                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message + '<ul>'+html+'</ul>',
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            })
        }
    </script>
@endsection