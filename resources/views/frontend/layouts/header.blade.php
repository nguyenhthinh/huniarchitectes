<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark fixed-top">
        <div class="container">
            @include('frontend.layouts.logo')
            @include('frontend.layouts.menu')
        </div>
    </nav>
</header>