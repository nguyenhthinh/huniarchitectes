<div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/{{$lang}}/introduction/"
               class="{{(Request::segment(2) == 'introduction') ? 'active': ''}}">Introduction</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{(Request::segment(2) == 'project') ? 'active': ''}}"
               href="{{route('project', ['lang'=>$lang])}}" id="navbarDropdownBlog" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                Projects
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                @if(Request::segment(2) == 'project')
                    <ul>
                        @foreach($proCats as $menu)
                            @if($menu->cat_name != 'uncategorized')
                                <a class="dropdown-item {{(\Request::segment(3) == $menu->project->alias->alias) ? 'active' : ''}}"
                                   href="{{route('project-cat',['lang'=>$lang, 'cat'=> $menu->project->alias->alias])}}">{{$menu->cat_name}}</a>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{(Request::segment(2) == 'project') ? 'active': ''}}"
               href="{{route('project', ['lang'=>$lang])}}" id="navbarDropdownBlog" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                News
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <ul>
                    @foreach($newsCats as $menu)
                        @if($menu->cat_name != 'uncategorized')
                            <a class="dropdown-item">{{$menu->cat_name}}</a>
                        @endif
                    @endforeach
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link {{(Request::segment(2) == 'contact') ? 'active': ''}}"
               href="{{route('contact',['lang'=>$lang])}}">Contact</a>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{route('project',['lang'=>'en'])}}">EN</a></li>
        <li class="nav-item"><a class="nav-link"  href="{{route('project',['lang'=>'fr'])}}">FR</a></li>
    </ul>
</div>

{{--<div class="container">--}}

{{--<ul>--}}
{{--    <li><a href="/{{$lang}}/introduction/" class="{{(Request::segment(2) == 'introduction') ? 'active': ''}}">+ INTRODUCTION</a></li>--}}
{{--    <li><a class='{{(Request::segment(2) == 'project') ? 'active': ''}}' href="{{route('project', ['lang'=>$lang])}}"> + PROJECTS </a>--}}
{{--        @if(Request::segment(2) == 'project')--}}
{{--            <ul>--}}
{{--                @foreach($proCats as $menu)--}}
{{--                    @if($menu->cat_name != 'uncategorized')--}}
{{--                        <li class="{{(\Request::segment(3) == $menu->project->alias->alias) ? 'active' : ''}}">--}}
{{--                            <a href="{{route('project-cat',['lang'=>$lang, 'cat'=> $menu->project->alias->alias])}}">{{$menu->cat_name}}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        @endif--}}
{{--    </li>--}}
{{--    <li style="display: none"><a class='{{(Request::segment(2) == 'news') ? 'active': ''}}' href="{{route('project', ['lang'=>$lang])}}"> + NEWS </a>--}}
{{--        @if(Request::segment(2) == 'news')--}}

{{--        @endif--}}
{{--    </li>--}}
{{--    <li><a href="{{route('project', ['lang'=>$lang])}}">+ DA</a></li>--}}
{{--    <li><a class="{{(Request::segment(2) == 'contact') ? 'active': ''}}" href="{{route('contact',['lang'=>$lang])}}">+ CONTACT</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
