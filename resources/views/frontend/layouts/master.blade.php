<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    @yield('meta')
    <link rel="icon" href="{{getOption('site', 'favicon')}}"/>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55858442-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link href={{ asset("js/bootstrap/css/bootstrap.min.css") }} rel="stylesheet"/>
    <link href={{ asset("css/modern-business.css") }} rel="stylesheet"/>
    <link href="{{ asset("css/fontawesome/css/all.css") }}" rel="stylesheet" type="text/css" />

    {{--<script type="text/javascript" src="/theme/js/jquery-1.8.2.min.js"></script>--}}
    {{--<script type="text/javascript" src="/theme/js/mt_ajax.js"></script>--}}
    {{--<script src="/theme/js/iepngfix_tilebg.js" type="text/javascript"></script>--}}

    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--var wdow = (window).innerHeight;--}}
            {{--var abc = $('#main-right').height();--}}
            {{--var def = $('.pro_image').width();--}}
            {{--$('.language').css("margin-top", wdow / 2);--}}
            {{--$('.main_left').css("height", abc + 60);--}}
            {{--$('.pro_item .pro_image img').css("height", def);--}}
        {{--});--}}
    {{--</script>--}}

    {{--<script type="text/javascript" src="/theme/js/init.js"></script>--}}

    {{--<link rel="Stylesheet" type="text/css" href="/theme/jQuery-smooth-scrolling/smoothDivScroll.css">--}}
    {{--<!-- jQuery library - Please load it from Google API's -->--}}
    {{--<script src="/theme/jQuery-smooth-scrolling/jquery_004.js" type="text/javascript"></script>--}}

    {{--<!-- jQuery UI Widget and Effects Core (custom download)--}}
         {{--You can make your own at: http://jqueryui.com/download -->--}}
    {{--<script src="/theme/jQuery-smooth-scrolling/jquery-ui-1.js" type="text/javascript"></script>--}}

    {{--<!-- Latest version of jQuery Mouse Wheel by Brandon Aaron--}}
         {{--You will find it here: http://brandonaaron.net/code/mousewheel/demos -->--}}
    {{--<script src="/theme/jQuery-smooth-scrolling/jquery_003.js" type="text/javascript"></script>--}}
    {{--<!-- Smooth Div Scroll 1.3 minified -->--}}
    {{--<script src="/theme/jQuery-smooth-scrolling/jquery_002.js" type="text/javascript"></script>--}}
    {{--<!-- Plugin initialization -->--}}
    {{--<script type="text/javascript">--}}
        {{--// Initialize the plugin with no custom options--}}
        {{--$(document).ready(function () {--}}
            {{--// I just set some of the options--}}
            {{--$("#makeMeScrollable").smoothDivScroll({--}}
                {{--mousewheelScrolling: "allDirections",--}}
                {{--manualContinuousScrolling: true,--}}
                {{--autoScrollingMode: "onStart"--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    {{--<script>--}}
        {{--$(document).ready(function (e) {--}}
            {{--var winH = window.innerHeight;--}}
            {{--var heighDiv = winH / 5 * 3;--}}
            {{--$('#makeMeScrollable').css("height", heighDiv);--}}
            {{--$('#makeMeScrollable img').css("height", heighDiv);--}}
        {{--});--}}
    {{--</script>--}}
    {{--<style type="text/css">--}}
        {{--#makeMeScrollable {--}}
            {{--padding: 4em 0 0;--}}
            {{--width: 100%;--}}
            {{--height: 35%;--}}
            {{--position: relative;--}}
        {{--}--}}

        {{--#makeMeScrollable div.scrollableArea * {--}}
            {{--position: relative;--}}
            {{--display: block;--}}
            {{--float: left;--}}
            {{--margin: 0;--}}
            {{--padding: 0;--}}
            {{--/* If you don't want the images in the scroller to be selectable, try the following--}}
               {{--block of code. It's just a nice feature that prevent the images from--}}
               {{--accidentally becoming selected/inverted when the user interacts with the scroller. */--}}
            {{---webkit-user-select: none;--}}
            {{---khtml-user-select: none;--}}
            {{---moz-user-select: none;--}}
            {{---o-user-select: none;--}}
            {{--user-select: none;--}}
        {{--}--}}

        {{--ul.pagination {--}}
            {{--text-align: right;--}}
        {{--}--}}

        {{--ul.pagination li {--}}
            {{--display: inline-block;--}}
        {{--}--}}

        {{--ul.pagination li a, ul.pagination li span {--}}
            {{--color: #b28e54;--}}
            {{--padding: 5px 10px;--}}
            {{--border: 1px solid;--}}
            {{--border-bottom: 1px solid;--}}
            {{--display: block;--}}
        {{--}--}}

        {{--ul.pagination li a:hover, ul.pagination li span:hover {--}}
            {{--background: #b28e54;--}}
            {{--color: #ffffff;--}}
            {{--cursor: pointer;--}}
        {{--}--}}
    {{--</style>--}}

    {{--<!-------  fancybox --------------->--}}
    {{--<script type="text/javascript" src="/theme/fancybox/source/jquery.fancybox.js?v=2.1.4"></script>--}}
    {{--<link rel="stylesheet" type="text/css" href="/theme/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen"/>--}}
    {{--<script type="text/javascript">--}}
        {{--jQuery(document).ready(function ($) {--}}
            {{--$(".fancybox").fancybox();--}}
        {{--});--}}
    {{--</script>--}}
    {{--<style type="text/css">--}}
        {{--.fancybox-custom .fancybox-skin {--}}
            {{--box-shadow: 0 0 50px #222;--}}
        {{--}--}}
    {{--</style>--}}

    {{--<style>--}}
        {{--#page_navigation .page_link {--}}
            {{--background-color: #fff;--}}
            {{--padding: 4px 12px 4px;--}}
            {{--margin: 2px 5px;--}}
        {{--}--}}

        {{--#page_navigation .active_page {--}}
            {{--background-color: #676767;--}}
            {{--color: #fff;--}}
            {{--padding: 4px 8px 4px;--}}
            {{--margin: 2px 5px;--}}
        {{--}--}}

        {{--#page_navigation a,--}}
        {{--#page_navigationdnn1 a,--}}
        {{--#page_navigationdnn2 a {--}}
            {{--border: 1px solid #a3a3a3;--}}
            {{---webkit-border-radius: 10px;--}}
            {{---moz-border-radius: 10px;--}}
            {{--border-radius: 10px;--}}
            {{--background-color: #fff;--}}
            {{--font-size: 15px;--}}
            {{--margin: 2px 5px;--}}
            {{--color: #000;--}}
            {{--text-decoration: none;--}}
            {{--font-weight: normal;--}}
            {{--padding: 4px 8px 4px;--}}
        {{--}--}}

        {{--#page_navigation a:hover,--}}
        {{--#page_navigationdnn1 a:hover,--}}
        {{--#page_navigationdnn2 a:hover {--}}
            {{--background-color: #676767;--}}
            {{--font-size: 15px;--}}
            {{--color: #fff !important;--}}
            {{--opacity: 0.9;--}}
        {{--}--}}

        {{--#page_navigation a.active_page,--}}
        {{--#page_navigationdnn1 a.active_pagednn1,--}}
        {{--#page_navigationdnn2 a.active_pagednn2 {--}}
            {{--font-size: 15px;--}}
            {{--color: #fff !important;--}}
            {{--opacity: 0.9;--}}
        {{--}--}}

        {{--.active_page,--}}
        {{--.active_pagednn1,--}}
        {{--.active_pagednn2 {--}}
            {{--font-size: 15px;--}}
            {{--color: #FBEB00 !important;--}}
            {{--opacity: 0.9;--}}
        {{--}--}}
    {{--</style>--}}

    @yield('styles')
</head>

<body>
@include('frontend.layouts.header')
@yield('container')
@yield('scripts')
@include('frontend.layouts.footer')
<script src={{ asset("js/jquery/jquery.min.js") }}></script>
<script src={{ asset("js/bootstrap/js/bootstrap.bundle.min.js") }}></script>

</body>

</html>
