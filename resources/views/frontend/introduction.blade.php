@extends('frontend.layouts.master')

@section('meta')
	<title>Introduction | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="title" content="{{getOption('site','meta_title')}}"/>
    <meta name="description" content="{{getOption('site','meta_description')}}"/>
    <meta name="keywords" content="{{getOption('site','keywords')}}"/>
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
@stop

@section('styles')

@stop

@section('container')
    <Div class="wraper">
        <div class="background">
            <div class="introduction">
                <div class="wrap text-wrap align-center">
                    <h1 class="widont balance-text" style="">Shinsegae International</h1>
                    <p>Seoul, South Korea</p>
                    <a href="#more">
                        <div class="read-button"><button class="read-more" tabindex="-1">Learn more</button></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="background-layer">
            <div class="introduction">
                <div class="wrap text-wrap align-center">
                    <h1 class="widont balance-text" style="">
                    The Jordan Schnitzer<br data-owner="balance-text">
                    Museum of Art at<br data-owner="balance-text">
                    Washington State&nbsp;University</h1>
                                            <p>Pullman, Washington</p>
                    <a href="#more">
                        <div class="read-button"><button class="read-more" tabindex="-1">Learn more</button></div>
                    </a>            
                </div>
            </div>
        </div>
        <div class="background-layer1">
            <div class="introduction">
                <div class="wrap text-wrap align-center">
                    <h1 class="widont balance-text" style="">
                    The Century Project<br data-owner="balance-text">for the Space&nbsp;Needle
                    </h1>						<p>Seattle, Washington</p>	
                    <a href="#more">
                        <div class="read-button"><button class="read-more" tabindex="-1">Learn more</button></div>
                    </a>				
				</div>
            </div>
        </div>
        <div id="more">
            <div class="info_lienhe" style="font-size: 16px; padding-top: 5em !important;"><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                {!! ($post)? $post->post_content : 'updating...' !!}
            </div>
        </div>
    </Div><!--wraper-->
    <div class="thoangbao_mgs" ><div id="luu_result"></div></div> <div class="popup_photo" id="popup_photo" >  <div id="show_thong_tin"></div></div>
    <div id="loading_result" style="z-index:999999;"></div><div id="khung_bg_view" ></div>
@stop

@section('scripts')
@stop