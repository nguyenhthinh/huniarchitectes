@extends('frontend.layouts.master')

@section('meta')
    <title>Project | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes"/>
    <meta name="title" content="{{getOption('site','meta_title')}}"/>
    <meta name="description" content="{{getOption('site','meta_description')}}"/>
    <meta name="keywords" content="{{getOption('site','keywords')}}"/>
    <meta name="copyright" content="huniarchitectes | huni |architectes"/>
    <meta http-equiv="expires" content="0"/>
    <meta name="resource-type" content="document"/>
    <meta name="distribution" content="global"/>
    <meta name="robots" content="index, follow"/>
    <meta name="revisit-after" content="1 days"/>
    <meta name="rating" content="general"/>
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8"/>
@stop

@section('styles')

@stop

@section('container')
    <div class="container">
        <div class="row">
            @php $i=1 @endphp
            @foreach($proCats as $item)
                @if($item->cat_name != 'uncategorized')
                    <div class="col-lg-4 mb-4 no-padding">
                        <div class="pro_item pro_{{$i++}}">
                            <div class="pro_image">
                                <a href="{{route('project-cat',['lang'=>$lang, 'cat'=> $item->project->alias->alias])}}"
                                   title="{{$item->cat_name}}">
                                    <img
                                            src="{{asset(showThumb($item->cat_image))}}"
                                            alt="{{$item->cat_name}}"/></a>
                            </div>
                            <div class="pro_name">
                            <h3>{{$item->cat_name}}
                            </h3>
                            </div>
                        </div>
                    </div>
            @endif
        @endforeach
        <!--pro_item-->
            <!--project_box-->
        {{--                </div>--}}

        {{--            </div>--}}
        <!--container-->
        </div>
        <!--wrap-->
    </Div>
    <!--wraper-->
    <div class="thoangbao_mgs">
        <div id="luu_result"></div>
    </div>
    <div class="popup_photo" id="popup_photo">
        <div id="show_thong_tin"></div>
    </div>
    <div id="loading_result" style="z-index:999999;"></div>
    <div id="khung_bg_view"></div>
@stop

@section('scripts')

@stop
