@extends('frontend.layouts.master')

@section('meta')
	<title>{{$category->cat_name}} | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="title" content="{{getOption('site','meta_title')}}"/>
    <meta name="description" content="{{getOption('site','meta_description')}}"/>
    <meta name="keywords" content="{{getOption('site','keywords')}}"/>
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
@stop

@section('styles')

@stop

@section('container')
<Div class="wraper">
    <div class="wrap">
        <div id="container">
            <div class="main_left">

                <div class="main_menu">
                    @include('frontend.layouts.menu')
                </div>
                <!--main_menu-->
            </div>
            <!--main_left-->
            <Div class="main_right" id="main-right">

                <div class="box project_box">
                    @include('frontend.layouts.logo')
                    @php $i=1; @endphp
                    @foreach($projects as $item)
                        <div class="pro_item pro_{{$i}}">
                            <div class="background">
                                <div class="background2">
                                    <div class="background3">
                                        <div class="box">
                                            <div class="pro_image">
                                                <a href="{{route('project-detail',['lang'=>$lang, 'cat'=> $category->project->alias->alias, 'post'=>$item->project->alias->alias])}}" title="{{$item->project->description}}"><img src="{{showThumb($item->post_image)}}" alt="{{$item->post_name}}" /></a>
                                            </div>
                                            <div class="pro_name">
                                                <h2>
                                                	<a style="letter-spacing: 0.3em;" href="{{route('project-detail',['lang'=>$lang, 'cat'=> $category->project->alias->alias, 'post'=>$item->project->alias->alias])}}">
                                                		{{$item->post_name}}<br/>
                                                		{{$item->project->location}} - {{$item->project->year}}
                                                	</a>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php $i++; @endphp
                    @endforeach
                    <!--pro_item-->
                    {{--<div class="navigation"><span class="current_page_item">1</span><span class="page_item"><a href="/projets/79/2/">2</a></span><span class="page_item"><a href="/projets/79/2/">»</a></span></div>--}}

                </div>
                {!! $projects->links() !!}
                <!--project_box-->
            </div>

        </div>
        <!--container-->
    </div>
    <!--wrap-->
</Div>
<!--wraper-->
<div class="thoangbao_mgs">
    <div id="luu_result"></div>
</div>
<div class="popup_photo" id="popup_photo">
    <div id="show_thong_tin"></div>
</div>
<div id="loading_result" style="z-index:999999;"></div>
<div id="khung_bg_view"></div>
@stop

@section('scripts')

@stop