@extends('frontend.layouts.master')

@section('meta')
	<title>huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="title" content="{{getOption('site','meta_title')}}"/>
    <meta name="description" content="{{getOption('site','meta_description')}}"/>
    <meta name="keywords" content="{{getOption('site','keywords')}}"/>
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
@stop

@section('styles')

@stop

@section('container')
<div class="wrap" style="position:relative;">
        <div class="language">
            <ul>
                <li><a href="{{route('project',['lang'=>'en'])}}">ENGLISH</a>|</li>
                <li><a style="margin:0 0 0 1em;" href="{{route('project',['lang'=>'fr'])}}">FRANCAIS</a></li>
            </ul>
        </div>
        <!--langguage-->
    </div>
    <!--wrap-->
@stop

@section('scripts')
    <script>
        setTimeout(function(){
            window.location.href = "{{route('project',['lang'=>'en'])}}";
        }, 1000);
    </script>
@stop