<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login-process','Api\AuthController@login')->name('api-login');
Route::get('/logout','Api\AuthController@logout')->name('api-logout');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'auth'], function(){
    Route::resource('user','Api\UserController',[
        'names' => [
            'index' => 'api-get-user'
        ]
    ]);
    Route::resource('skill','Api\SkillController',[
        'names' => [
            'index' => 'api-get-skill'
        ]
    ]);
    Route::resource('emplv','Api\EmpLevelController',[
        'names' => [
            'index' => 'api-get-emplevel'
        ]
    ]);
    Route::resource('role','Api\RoleController',[
        'names' => [
            'index' => 'api-get-role'
        ]
    ]);
    Route::resource('project','Api\ProjectController');
});

