<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

/*Route::get('/','CommonController@index')->name('home');
Route::get('/login','AuthController@login')->name('login');

Route::group(['middleware'=>'auth'], function(){
    Route::get('/projects','CommonController@projects')->name('project-main');
    Route::get('/projects/detail/{id}','CommonController@projectDetail')->name('project-detail');


    //Administrator
    Route::group(['prefix'=>'manage','middleware'=>['role:admin']], function(){
        Route::get('/user','ManagerController@user')->name('manage-user');
    });
});*/
Route::get('language/{lang}', function($lang){
//   $langObj = getLangID($lang);
    \Session::put('language', $lang);
   return redirect()->back();
})->name('changelang');




Route::group(['prefix'=>'administrator', 'namespace'=>'Administrator'], function(){
    Route::get('/login', 'AuthController@login')->name('admin-login');
    Route::post('/login-process', 'AuthController@loginProcess')->name('admin-login-process');
    Route::get('/logout', 'AuthController@logout')->name('admin-logout');

    Route::group(['middleware'=>['dashboard','language']], function(){
        Route::get('/', 'DashboardController@index')->name('admin-home');

        //Upload
        Route::post('/upload','FileController@upload')->name('admin-upload');
        Route::post('/upload/input','FileController@uploadInput')->name('admin-upload-input');
        Route::get('/upload/browser','FileController@browser')->name('admin-upload-browser');

        //Danh mục
        Route::get('/category/{type}','CategoryController@index')->name('admin-post-cat');
        Route::post('/category/{type}/add','CategoryController@add')->name('admin-post-cat-add');
        Route::post('/category/{type}/quick-change','CategoryController@quickChange')->name('admin-post-cat-quick-change');
        Route::post('/category/{type}/edit','CategoryController@edit')->name('admin-post-cat-edit');
        Route::post('/category/{type}/delete','CategoryController@delete')->name('admin-post-cat-delete');
        Route::post('/category/{type}/reorder','CategoryController@reOrder')->name('admin-post-cat-change-order');

        //POST
        Route::get('/post/{type}','PostController@index')->name('admin-post-list');
        Route::get('/post/{type}/add','PostController@add')->name('admin-post-add');
        Route::post('/post/{type}/add/process','PostController@addProcess')->name('admin-post-add-process');
        Route::get('/post/{type}/edit/{id}','PostController@edit')->name('admin-post-edit');
        Route::post('/post/{type}/quick-change','PostController@quickChange')->name('admin-post-quick-change');
        Route::post('/post/{type}/edit/process','PostController@editProcess')->name('admin-post-edit-process');
        Route::post('/post/{type}/delete','PostController@delete')->name('admin-post-delete');

        //Setting
        Route::get('/setting', 'DashboardController@setting')->name('admin-setting');
        Route::post('/setting/update', 'DashboardController@settingUpdate')->name('admin-setting-save');
        Route::get('/change-pass', 'DashboardController@changepass')->name('admin-change-pass');
        Route::post('/change-pass', 'DashboardController@changepassProcess')->name('admin-change-pass-process');
    });
});
// Auth::routes();


Route::get('/', 'CommonController@index')->name('home');
Route::get('/home', 'CommonController@index');

Route::group(['middleware'=>'frontend'], function(){
    Route::get('/{lang}/project', 'CommonController@project')->name('project');
    Route::get('/{lang}/project/{cat}', 'CommonController@projectCategory')->name('project-cat');
    Route::get('/{lang}/project/{cat}/{post}.html', 'CommonController@projectDetail')->name('project-detail');

	Route::get('/{lang}/news', 'CommonController@news')->name('news');
	Route::get('/{lang}/news/{cat}', 'CommonController@newsCategory')->name('news-cat');
	Route::get('/{lang}/news/{cat}/{article}.html', 'CommonController@newsDetail')->name('news-detail');
    Route::get('/{lang}/contact', 'CommonController@contact')->name('contact');
    Route::get('/{lang}/introduction', 'CommonController@introduction')->name('introduction');
});

Route::get('/convert/parent', 'ConvertController@parent');
Route::get('/convert/child', 'ConvertController@child');

Route::post('/sendMail', 'CommonController@sendMail');
