<?php $__env->startSection('title', 'Danh sách '.getTypeName($type)); ?>

<?php $__env->startSection('content'); ?>
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"><?php echo e(getTypeName($type)); ?></span>
            <h3 class="page-title">Danh sách <?php echo e(getTypeName($type)); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <form class="row">
                        <div class="col">
                            <input class="form-control" placeholder="Tìm theo tên" name="name" value="<?php echo e($request->name); ?>"/>
                        </div>
                        <div class="col">
                            <select class="form-control" name="parent">
                                <option value="0">Tìm theo mục cha</option>
                                <?php $__currentLoopData = $cates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->cat_id); ?>" <?php echo e(($request->parent == $category->cat_id)? 'selected': ''); ?>><?php echo e($category->cat_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-control" name="orderBy">
                                <option value="">Sắp xếp</option>
                                <option <?php echo e(($request->orderBy == 'post_id-desc')? 'selected': ''); ?> value="post_id-desc">Mới nhất</option>
                                <option <?php echo e(($request->orderBy == 'post_id-asc')? 'selected': ''); ?> value="post_id-asc">Cũ nhất</option>
                            </select>
                        </div>
                        <div class="col">
                            <button type="submit" class="mb-2 btn btn-sm btn-success mr-1">Lọc danh mục</button>
                        </div>
                        <div class="col">
                            <button onclick="themPost()" type="button" class="mb-2 btn btn-sm btn-primary mr-1">Thêm <?php echo e(getTypeName($type)); ?></button>
                        </div>
                    </form>
                </div>
                <div class="card-body p-0 pb-3">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0">Tên <?php echo e(getTypeName($type)); ?></th>
                                <th scope="col" class="border-0">Đường dẫn</th>
                                <th scope="col" class="border-0">Mục cha</th>
                                <th scope="col" class="border-0">Trạng thái</th>
                                <th scope="col" class="border-0">Thứ tự</th>
                                <th scope="col" class="border-0">Ngày đăng</th>
                                <th scope="col" class="border-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($post->post_id); ?></td>
                                    <td><img src="<?php echo e(showThumb($post->post_image)); ?>" width="80"/></td>
                                    <td><?php echo e($post->post_name); ?></td>
                                    <td>
                                        <?php
                                            if($type == 'post') echo $post->alias->alias;
                                            if($type == 'project') echo $post->project->alias->alias;
                                        ?>
                                    </td>
                                    <td><?php echo e($post->category->cat_name); ?></td>
                                    <td>
                                        <select class="form-control" onchange="quickChange(this, 'status', <?php echo e($post->post_id); ?>)">
                                            <option value="1" <?php if($post->post_publish): ?> selected <?php endif; ?>>Hiển thị</option>
                                            <option value="0" <?php if(!$post->post_publish): ?> selected <?php endif; ?>>Không hiển thị</option>
                                        </select>
                                    </td>
                                    <td><input class="form-control" onchange="quickChange(this, 'order', <?php echo e($post->post_id); ?>)" size="2" value="<?php echo e($post->post_order); ?>"/></td>
                                    <td><?php echo e($post->created_at); ?></td>
                                    <td>
                                        <a href="<?php echo e(route('admin-post-edit',['type'=>$type, 'id'=>$post->post_id])); ?>"><button  class="mb-2 btn btn-sm btn-warning mr-1"><i class="material-icons">edit</i> Chỉnh sửa</button></a>
                                        <button class="mb-2 btn btn-sm btn-danger mr-1" onclick="deletePost(<?php echo e($post->post_id); ?>)"><i class="material-icons">delete</i> Xóa</button>
                                    </td>

                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                        <?php echo $posts->appends(request()->query())->links(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    function quickChange(obj, action, id){
        console.log(obj.value, action, id);
        $.ajax({
                url: '<?php echo e(route('admin-post-quick-change',['type'=>$type])); ?>',
                data: {_token: '<?php echo e(csrf_token()); ?>', id: id, action: action, value: obj.value},
                type: 'post'
            }).then(function (res) {
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.reload();
                    }
                });
            }).catch(function (e) {
                console.log(e); //alert(e.responseText)

                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message,
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            });
    }
    
    function themPost() {
        window.location.href = '<?php echo e(route('admin-post-add',['type'=>$type])); ?>'
    }

    function deleteCat(i){
        var confirmed = confirm('Bạn có chắc chắn muốn xóa đối tượng này?')
        if(confirmed){
            $.ajax({
                url: '<?php echo e(route('admin-post-cat-delete',['type'=>$type])); ?>',
                data: {_token: '<?php echo e(csrf_token()); ?>', id: i},
                type: 'post'
            }).then(function (res) {
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.reload();
                    }
                });
            }).catch(function (e) {
                console.log(e); //alert(e.responseText)

                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message,
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            });
        }

    }
    function deletePost(i){
        var confirmed = confirm('Bạn có chắc chắn muốn xóa đối tượng này?')
        if(confirmed){
            $.ajax({
                url: '<?php echo e(route('admin-post-delete',['type'=>$type])); ?>',
                data: {_token: '<?php echo e(csrf_token()); ?>', id: i},
                type: 'post'
            }).then(function (res) {
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.reload();
                    }
                });
            }).catch(function (e) {
                console.log(e); //alert(e.responseText)

                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message,
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            });
        }

    }
    function removeInValid(id){
        $('#input_'+id).removeClass('is-invalid')
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>