<ul>
    <li><a href="/<?php echo e($lang); ?>/introduction/" class="<?php echo e((Request::segment(2) == 'introduction') ? 'active': ''); ?>">+ INTRODUCTION</a></li>
    <li><a class='<?php echo e((Request::segment(2) == 'project') ? 'active': ''); ?>' href="<?php echo e(route('project', ['lang'=>$lang])); ?>"> + PROJECTS </a>
        <?php if(Request::segment(2) == 'project'): ?>
            <ul>
                <?php $__currentLoopData = $proCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($menu->cat_name != 'uncategorized'): ?>
                        <li class="<?php echo e((\Request::segment(3) == $menu->project->alias->alias) ? 'active' : ''); ?>">
                            <a href="<?php echo e(route('project-cat',['lang'=>$lang, 'cat'=> $menu->project->alias->alias])); ?>"><?php echo e($menu->cat_name); ?></a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        <?php endif; ?>
    </li>
    <li style="display: none"><a class='<?php echo e((Request::segment(2) == 'news') ? 'active': ''); ?>' href="<?php echo e(route('project', ['lang'=>$lang])); ?>"> + NEWS </a>
        <?php if(Request::segment(2) == 'news'): ?>

        <?php endif; ?>
    </li>
    <li><a href="<?php echo e(route('project', ['lang'=>$lang])); ?>">+ DA</a></li>
    <li><a class="<?php echo e((Request::segment(2) == 'contact') ? 'active': ''); ?>" href="<?php echo e(route('contact',['lang'=>$lang])); ?>">+ CONTACT</a></li>
</ul>