<?php $__env->startSection('meta'); ?>
	<title><?php echo e($project->post_name); ?> | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="keywords" content="huniarchitectes | huni |architectes" />
    <meta name="description" content="huniarchitectes | huni |architectes" />
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
<div class="wraper">
    <div class="wrap">
        <div id="container">
            <div class="main_right" id="main-right" style="padding: 0;">
                <div class="box project_box">
                    <?php echo $__env->make('frontend.layouts.logo', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
            <div style="clear: both;"></div>
            <?php if($project->project->video): ?>
            <style>
                #youtube {
                    padding-top: 52px;
                }
            </style>
            <div id="youtube">
    			<div id="player"></div>
			</div>
			<script src="http://www.youtube.com/player_api" > </script>
			<script >
			    function gup(name, url) {
			            if (!url) url = location.href;
			            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
			            var regexS = "[\\?&]" + name + "=([^&#]*)";
			            var regex = new RegExp(regexS);
			            var results = regex.exec(url);
			            return results == null ? null : results[1];
			        }
			    var youtbID = gup('v', '<?php echo e($project->project->video); ?>');
			    if(youtbID == null){
			        var urlvideo = '<?php echo e($project->project->video); ?>';
			        youtbID = urlvideo.replace("https://youtu.be/","");
			    }
			    //alert(youtbID);
			    // create youtube player
			    var player;
			    
			    function onYouTubePlayerAPIReady() {
			        var h = $('#makeMeScrollable').height();
			        //alert(h);
			        player = new YT.Player('player', {
			            height: h,
			            width: '100%',
			            videoId: youtbID,
			            playerVars:{rel:0},
						showinfo: 0,
						rel: 0,
			            events: {
			                'onReady': onPlayerReady,
			                'onStateChange': onPlayerStateChange
			            }
			        });
			    }
			    
			    // autoplay video
			    function onPlayerReady(event) {
			        event.target.playVideo();
			    }
			    
			    // when video ends
			    function onPlayerStateChange(event) {
			        if (event.data === 0) {
			            loadSlide();
			        }
			    }
			    function loadSlide(){
			    	player.pauseVideo()
 
			        $('#youtube').hide();
			        $('#overloadvideo').show();
			        $("#makeMeScrollable").smoothDivScroll({
							mousewheelScrolling: "allDirections",
							manualContinuousScrolling: true,
							autoScrollingMode: "onStart"
						});
			        $('#makeMeScrollable').css("height",heighDiv);
					$('#makeMeScrollable img').css("height",heighDiv);
			        $(".fancybox").fancybox();
			        
			    }
			    function showVideo(){
			        // player = new YT.Player('player', {
			        //     height: '500',
			        //     width: '100%',
			        //     videoId: youtbID,
			        //     events: {
			        //         'onReady': onPlayerReady,
			        //         'onStateChange': onPlayerStateChange
			        //     }
			        // });
			        player.playVideo()
 
			        $('#overloadvideo').hide();
			         $('#youtube').show();
			    }
			</script>
            <?php endif; ?>
			<div id="overloadvideo" style="<?php echo e(($project->project->video)? "display:none":""); ?>">
	            <div id="makeMeScrollable">
	                <div style="display: block; opacity: 0.0985047;" class="scrollingHotSpotLeft scrollingHotSpotLeftVisible"></div>
	                <div style="opacity: 0.0985047;" class="scrollingHotSpotRight scrollingHotSpotRightVisible"></div>
	                <div class="scrollWrapper">
	                    <div style="width: 3784px;" class="scrollableArea">
                            <?php
                                $photos = explode(',', $project->project->gallery);
                            ?>
                            <?php $__currentLoopData = $photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                        <a class="fancybox" data-fancybox-group="gallery" href="<?php echo e(imageUrl($photo)); ?>"><img src="<?php echo e(imageUrl($photo)); ?>" alt="" id="gnome"></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                    </div>
	                </div>
	            </div>
       		</div>
       		<div class="info">

                <div class="left">
                    <p>PROJECT: <?php echo e($project->post_name); ?></p>
                    <p>LOCATION: <?php echo e($project->project->location); ?></p>
                    <p>YEAR: <?php echo e($project->project->year); ?></p>
                    <p>AREA: <?php echo e($project->project->area); ?></p>
                    <p>COST: <?php echo e($project->project->cost); ?></p>
                    <p><?php echo e($project->project->designer); ?></p>
                </div>
                <div class="right">
                    <a href="javascript:function(){window.location.reload()}" style="<?php echo e((!$project->project->video)? "display:none":""); ?>">RETOUR</a>
                    <a href="javascript:void();" onclick="showVideo()" style="<?php echo e((!$project->project->video)? "display:none":""); ?>">| PLAY VIDEO |</a>
                    <a href="javascript:void();" onclick="loadSlide()" style="<?php echo e((!$project->project->video)? "display:none":""); ?>">SLIDESHOW</a>
                </div>
            </div>
            <!--info-->
        <!--container-->
    </div>
    <!--wrap-->
</Div>
<!--wraper-->
<div class="thoangbao_mgs">
    <div id="luu_result"></div>
</div>
<div class="popup_photo" id="popup_photo">
    <div id="show_thong_tin"></div>
</div>
<div id="loading_result" style="z-index:999999;"></div>
<div id="khung_bg_view"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>