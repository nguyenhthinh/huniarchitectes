<?php $__env->startSection('title', 'Cấu hình trang'); ?>

<?php $__env->startSection('content'); ?>
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Cấu hình</span>
            <h3 class="page-title">Cấu hình trang</h3>
        </div>
    </div>
    <form id="settingForm" class="row">
        <?php echo csrf_field(); ?>
        <div class="col-sm-6">
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Thông tin chung</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Tên trang</div>
                        <div class="col-9"><input class="form-control" name="option[site][name]" value="<?php echo e(getOption('site','name')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">META_title</div>
                        <div class="col-9"><input class="form-control" name="option[site][meta_title]" value="<?php echo e(getOption('site','meta_title')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">META_description</div>
                        <div class="col-9"><input class="form-control" name="option[site][meta_description]" value="<?php echo e(getOption('site','meta_description')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-2">Logo</div>
                        <div class="col-sm-4">
                            <!-- <input id="uploadLogo" name="option[site][logo]"  value="<?php echo e(getOption('site','logo')); ?>"/> -->
                            <div class="ktu-input-wrapper" id="featureSection">
                                <div class="ktu-item" id="ktu-item-logo_site">
                                        <span class="ktu-select-btn" onclick="KTUChooseImage('logo_site')" id="ktu-add-btn-logo_site">CHOOSE IMAGE</span>
                                        <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-logo_site" src="<?php echo e(getOption('site','logo')); ?>"></div>
                                        <input type="hidden" name="option[site][logo]" value="<?php echo e(getOption('site','logo')); ?>" id="ktu-image-logo_site">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">Favicon</div>
                        <div class="col-sm-4">
                            <!-- <input id="uploadFavicon" name="option[site][favicon]" value="<?php echo e(getOption('site','favicon')); ?>"/> -->
                            <div class="ktu-input-wrapper" id="featureSection">
                                <div class="ktu-item" id="ktu-item-favicon">
                                        <span class="ktu-select-btn" onclick="KTUChooseImage('favicon')" id="ktu-add-btn-favicon">CHOOSE IMAGE</span>
                                        <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-favicon" src="<?php echo e(getOption('site','favicon')); ?>"></div>
                                        <input type="hidden" name="option[site][favicon]" value="<?php echo e(getOption('site','favicon')); ?>" id="ktu-image-favicon">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Thông tin chủ sở hữu</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Công ty</div>
                        <div class="col-9"><input class="form-control" name="option[author][name]" value="<?php echo e(getOption('author','name')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Địa chỉ</div>
                        <div class="col-9"><input class="form-control" name="option[author][address]" value="<?php echo e(getOption('author','address')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Số điện thoại</div>
                        <div class="col-9"><input class="form-control" name="option[author][phone]" value="<?php echo e(getOption('author','phone')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Email</div>
                        <div class="col-9"><input class="form-control" name="option[author][email]" value="<?php echo e(getOption('author','email')); ?>"/></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card card-small mb-4">
                <div class="card-header">
                    <h5>Mạng xã hội</h5>
                </div>
                <div class="card-body p-3">
                    <div class="row mb-3">
                        <div class="col-3">Facebook</div>
                        <div class="col-9"><input class="form-control" name="option[social][facebook]" value="<?php echo e(getOption('social','facebook')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Twitter</div>
                        <div class="col-9"><input class="form-control" name="option[social][twitter]" value="<?php echo e(getOption('social','twitter')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Youtube</div>
                        <div class="col-9"><input class="form-control" name="option[social][youtube]" value="<?php echo e(getOption('social','youtube')); ?>"/></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Instagram</div>
                        <div class="col-9"><input class="form-control" name="option[social][instagram]" value="<?php echo e(getOption('social','instagram')); ?>"/></div>
                    </div>
                </div>
            </div>
            <div class="card card-small mb-4">

                <div class="card-body p-3">
                    <div class="row">
                        <div class="col">
                            <button onclick="saveSetting()" type="button" class="btn btn-primary btn-block">Lưu lại</button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
<input type="hidden" id="setimage"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="/plugins/ktupload/css/ktupload.min.css"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="/plugins/ckfinder/ckfinder.js"></script>
<script src="/plugins/ktupload/js/ktupload2.js"></script>
    <script>
        // $('#uploadLogo').KTUInputUpload({
        //     curent_item: "<?php echo e(showThumb(getOption('site','logo'))); ?>",
        //     upload_url: '<?php echo e(route('admin-upload-input')); ?>?_token=<?php echo e(csrf_token()); ?>',
        //     loading_icon : '/plugins/ktupload/img/loadingsvg.svg'
        // })
        // $('#uploadFavicon').KTUInputUpload({
        //     curent_item: "<?php echo e(showThumb(getOption('site','favicon'))); ?>",
        //     upload_url: '<?php echo e(route('admin-upload-input')); ?>?_token=<?php echo e(csrf_token()); ?>',
        //     loading_icon : '/plugins/ktupload/img/loadingsvg.svg'
        // })

        function saveSetting(){
            var formData = $('#settingForm').serialize()
            $.ajax({
                url: '<?php echo e(route('admin-setting-save')); ?>',
                data: formData,
                type: 'post'
            }).then(function(res){
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.reload();
                    }
                });
            }).catch(function(e){
                var html = ''
                $.each(e.responseJSON.errors, function(k, item){
                    console.log(k, item)
                    html+= '<li>'+k+': '+item[0]+'</li>';
                })
                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message + '<ul>'+html+'</ul>',
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            })
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>