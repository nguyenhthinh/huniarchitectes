<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark fixed-top">
        <div class="container">
            <?php echo $__env->make('frontend.layouts.logo', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('frontend.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </nav>
</header>