<?php $__env->startSection('title', 'Chỉnh sửa '.getTypeName($type)); ?>

<?php $__env->startSection('content'); ?>

<!-- / .main-navbar -->
<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"><?php echo e(getTypeName($type)); ?></span>
            <h3 class="page-title">Chỉnh sửa <?php echo e(getTypeName($type)); ?></h3>
        </div>
    </div>
    <!-- End Page Header -->
    <form id="postForm" class="row" method="post" action="<?php echo e(route('admin-post-add-process',['type'=>'post'])); ?>">
        <?php echo csrf_field(); ?>
        <input type="hidden" name="post_id" value="<?php echo e($post->post_id); ?>"/>
        <div class="col-lg-9 col-md-12">
            
            <!-- Add New Post Form -->
            <div class="card card-small mb-3">
                <div class="card-body">
                    <div class="add-new-post">
                        <input class="form-control form-control-lg mb-3" value="<?php echo e($post->post_name); ?>" name="name" id="title" type="text" onkeyup="change_alias('#title', '#alias')" placeholder="Tiêu đề <?php echo e(getTypeName($type)); ?>">
                        <input class="form-control form-control-lg mb-3"  value="<?php echo e($post->$type->alias->alias); ?>" name="alias" id="alias" type="text" placeholder="Đường dẫn <?php echo e(getTypeName($type)); ?>">
                        <?php if($type != 'project'): ?>
                        <textarea name="description" placeholder="Tóm tắt" class="form-control mb-3"><?php echo $post->post_expert; ?>"</textarea><br/>
                        <textarea name="content" class="form-control mb-3"><?php echo $post->post_content; ?></textarea><br/>
                        <?php endif; ?>
                        <input class="form-control form-control-lg mb-3" value="<?php echo e($post->post_meta_title); ?>" name="meta_title" placeholder="SEO Meta title"/>
                        <input class="form-control form-control-lg mb-3" value="<?php echo e($post->post_meta_description); ?>" name="meta_description" placeholder="SEO Meta description"/>
                    </div>
                </div>
            </div>
            <?php if($type == 'project'): ?>
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Gallery</h6>
                    </div>
                    <div class='card-body p-0'>
                        <div class="row-fluid border-0">
                            <div class="col-sm-12">

                                <div id="uploadSection" class="pt-3"></div>
                                <div class="ktu-wrapper" id="gallerySection">
                                        <?php 
                                            $gallery = explode(',', $post->project->gallery); $i=1;
                                        ?>
                                        <div class="ktu-item" id="ktu-add-item">
                                                <span class="ktu-select-btn" onclick="KTUAddItem()">ADD IMAGE</span>
                                                <div class="ktu-thumb"><img src="/plugins/ktupload/img/add-photo.png"></div>
                                                <input type="hidden" value="<?php echo e(count($gallery) + 1); ?>" id="galCount"/>
                                                <input type="hidden" value="" id="setimage"/>
                                        </div>
                                        
                                        <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="ktu-item" id="ktu-item-<?php echo e($i); ?>">
                                                <span class="ktu-select-btn" onclick="KTUChooseImage(<?php echo e($i); ?>)" id="ktu-add-btn-<?php echo e($i); ?>">CHOOSE IMAGE</span>
                                                <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-<?php echo e($i); ?>" src="<?php echo e($g); ?>"></div>
                                                <span class="ktu-remove-btn ktu-show" id="ktu-remove-btn-<?php echo e($i); ?>" onclick="KTUDeleteItem(<?php echo e($i); ?>)">x</span>
                                                <input type="hidden" name="project_gallery[]" value="<?php echo e($g); ?>" id="ktu-image-<?php echo e($i); ?>">
                                        </div>
                                        <?php $i++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <br/>
                                <input class="form-control form-control-lg mb-3" name="project_video" value="<?php echo e($post->project->video); ?>" placeholder="Video"/>
                                <input class="form-control form-control-lg mb-3" name="project_year" placeholder="Year" value="<?php echo e($post->project->year); ?>"/>
                                <input class="form-control form-control-lg mb-3" name="project_location" placeholder="Location" value="<?php echo e($post->project->location); ?>"/>
                                <input class="form-control form-control-lg mb-3" name="project_area" placeholder="Area" value="<?php echo e($post->project->area); ?>"/>
                                <input class="form-control form-control-lg mb-3" name="project_cost" placeholder="Cost" value="<?php echo e($post->project->cost); ?>"/>
                                <input class="form-control form-control-lg mb-3" name="project_designer" placeholder="Designer" value="<?php echo e($post->project->designer); ?>"/>
                                <input class="form-control form-control-lg mb-3" name="project_description" placeholder="Description" value="<?php echo e($post->project->description); ?>"/>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endif; ?>
            <!-- / Add New Post Form -->
        </div>
        <div class="col-lg-3 col-md-12">

            <!-- Post Overview -->
            <div class='card card-small mb-3'>
                <div class="card-header border-bottom">
                    <h6 class="m-0">Cơ bản</h6>
                </div>
                <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item">
                            <label>Hình ảnh</label>
                            <div class="ktu-input-wrapper" id="featureSection">
                                <div class="ktu-item" id="ktu-item-feature">
                                        <span class="ktu-select-btn" onclick="KTUChooseImage('feature')" id="ktu-add-btn-feature">CHOOSE IMAGE</span>
                                        <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-feature" src="<?php echo e($post->post_image); ?>"></div>
                                        <input type="hidden" name="image" value="<?php echo e($post->post_image); ?>" id="ktu-image-feature">
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <select class="form-control" name="parent">
                                <?php $__currentLoopData = $cates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->cat_id); ?>" <?php echo e(($post->post_category == $category->cat_id)? 'selected': ''); ?>><?php echo e($category->cat_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </li>
                        <li class="list-group-item d-flex px-3">
                            
                            <button class="btn btn-sm btn-accent ml-auto" type="button" onclick="savePost()">
                                <i class="material-icons">file_copy</i> Publish</button>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- / Post Overview -->
            <!-- Post Overview -->

            <!-- / Post Overview -->
        </div>
    </form>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="/plugins/ktupload/css/ktupload.min.css"/>
    <style>
        .ktu-wrapper .ktu-item .ktu-remove-btn {
            position: absolute;
            top: -5px;
            right: -5px;
            padding: 10px;
            background: red;
            color: #FFF;
            font-weight: bold;
            cursor: pointer;
            border-radius: 50%;
            height: 20px;
            width: 20px;
            text-align: center;
            z-index: 3;
            line-height: 0px;
            text-align: center;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="/plugins/ckfinder/ckfinder.js"></script>
<script src="/plugins/ktupload/js/ktupload2.js"></script>
    <script>
        function change_alias(title, alias) {
            var str = $(title).val();
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str = str.replace(/đ/g,"d");
            str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,"");
            str = str.replace(/ + /g,"-");
            str = str.replace(/ /g,"-");
            str = str.trim();
            $(alias).val(str)
        }
    </script>
    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script>
<?php if($type != 'project'): ?>
        CKEDITOR.editor.fileUploadRequest = function( evt ) {
            var fileLoader = evt.data.fileLoader,
                formData = new FormData(),
                xhr = fileLoader.xhr;

            xhr.open( 'PUT', fileLoader.uploadUrl, true );
            xhr.setRequestHeader('X-CSRF-TOKEN', '<?php echo e(csrf_token()); ?>');
            formData.append( 'upload', fileLoader.file, fileLoader.fileName );
            fileLoader.xhr.send( formData );

            // Prevented the default behavior.
            evt.stop();
        }
        CKEDITOR.replace( 'content', {
            uploadUrl: '<?php echo e(route('admin-upload')); ?>?_token=<?php echo e(csrf_token()); ?>',
            filebrowserBrowseUrl: '<?php echo e(route('admin-upload-browser')); ?>?_token=<?php echo e(csrf_token()); ?>',
            filebrowserUploadUrl: '<?php echo e(route('admin-upload')); ?>?_token=<?php echo e(csrf_token()); ?>'
        } );
        <?php endif; ?>

        <?php if($type == 'project'): ?>
        var gl_photo_item = `<div class="ktu-item" id="ktu-item-{ID}">
                                <span class="ktu-select-btn" onclick="KTUChooseImage({ID})" id="ktu-add-btn-{ID}">CHOOSE IMAGE</span>
                                <div class="ktu-thumb"><img class="ktu-hidex" id="ktu-thumb-{ID}" src="/plugins/ktupload/img/default-image.png"></div>
                                <span class="ktu-remove-btn ktu-show" id="ktu-remove-btn-{ID}" onclick="KTUDeleteItem({ID})">x</span>
                                <input type="hidden" name="project_gallery[]" value="" id="ktu-image-{ID}">
                            </div>`;
        function KTUAddItem(){
            var i = $('#galCount').val();
            i++;
            var addItem = gl_photo_item;
            addItem = addItem.replace(/{ID}/g, i);
            console.log(addItem);
            $('#gallerySection').append(addItem);
            $('#galCount').val(i);
        }
        <?php endif; ?>

        function savePost(){
            //$('#postForm').submit()
            <?php if($type != 'project'): ?>
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            <?php endif; ?>
            var formData = $('#postForm').serialize()
            $.ajax({
                url: '<?php echo e(route('admin-post-edit-process',['type'=>$type])); ?>',
                data: formData,
                type: 'post'
            }).then(function (res) {
                $.toast({
                    heading: 'Thao tác thành công',
                    text: res.message,
                    hideAfter: 500,
                    icon: 'success',
                    afterHidden: function afterHidden() {
                        window.location.href = '<?php echo e(route('admin-post-list',['type'=>$type])); ?>';
                    }
                });
            }).catch(function (e) {
                console.log(e); //alert(e.responseText)
                var html = ''
                $.each(e.responseJSON.errors, function(k, item){
                    console.log(k, item)
                    html+= '<li>'+k+': '+item[0]+'</li>';
                })
                $.toast({
                    heading: 'Lỗi',
                    text: e.responseJSON.message + '<ul>'+html+'</ul>',
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: false
                });
            });
        }
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>