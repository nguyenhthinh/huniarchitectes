<div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/<?php echo e($lang); ?>/introduction/"
               class="<?php echo e((Request::segment(2) == 'introduction') ? 'active': ''); ?>">Introduction</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle <?php echo e((Request::segment(2) == 'project') ? 'active': ''); ?>"
               href="<?php echo e(route('project', ['lang'=>$lang])); ?>" id="navbarDropdownBlog" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                Projects
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <?php if(Request::segment(2) == 'project'): ?>
                    <ul>
                        <?php $__currentLoopData = $proCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($menu->cat_name != 'uncategorized'): ?>
                                <a class="dropdown-item <?php echo e((\Request::segment(3) == $menu->project->alias->alias) ? 'active' : ''); ?>"
                                   href="<?php echo e(route('project-cat',['lang'=>$lang, 'cat'=> $menu->project->alias->alias])); ?>"><?php echo e($menu->cat_name); ?></a>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                <?php endif; ?>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link dropdown-toggle <?php echo e((Request::segment(2) == 'project') ? 'active': ''); ?>"
               href="<?php echo e(route('project', ['lang'=>$lang])); ?>" id="navbarDropdownBlog" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                News
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php echo e((Request::segment(2) == 'contact') ? 'active': ''); ?>"
               href="<?php echo e(route('contact',['lang'=>$lang])); ?>">Contact</a>
        </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('project',['lang'=>'en'])); ?>">EN</a></li>
        <li class="nav-item"><a class="nav-link"  href="<?php echo e(route('project',['lang'=>'fr'])); ?>">FR</a></li>
    </ul>
</div>



























