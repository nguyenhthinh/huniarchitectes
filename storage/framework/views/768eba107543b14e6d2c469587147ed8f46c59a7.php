<?php $__env->startSection('meta'); ?>
	<title>Project | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="title" content="<?php echo e(getOption('site','meta_title')); ?>"/>
    <meta name="description" content="<?php echo e(getOption('site','meta_description')); ?>"/>
    <meta name="keywords" content="<?php echo e(getOption('site','keywords')); ?>"/>
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
    <Div class="wraper">
        <div class="wrap">
        <div id="container">
            <div class="main_left">

                <div class="main_menu">
                    <?php echo $__env->make('frontend.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <!--main_menu-->
            </div>
            <!--main_left-->

            <Div class="main_right" id="main-right">
                <div class="box project_box">

                    <div class="info_lienhe">
                        <div class="info_france">
                            <?php echo $post->post_content; ?>

                        </div>
                        <div class="info_vietnam">
                            <?php echo $viePost->post_content; ?>

                        </div>
                    </div>

                    <div class="social_link">
                        <span><a class="fab fa-facebook-square" target="_blank" href="https://www.facebook.com/huniarchitectes/?__tn__=%2Cd%3C-R&eid=ARB-FdWCFn-wzw9WsGWw00pNHYQXimaWyBbvKPVxXI_aCvPNvELZFodI_VK7vVSU2lASvEkQWCukUdT5"></a></span>
                        <span><a class="fab fa-linkedin" target="_blank" href="https://vn.linkedin.com/in/huni-architectes-a9584a157?challengeId=AQGglNu97wJNSAAAAW-m5Om51FjsNl7k7cuO4GHnSfCcMj9Fl0kVrnx6JrCsm1hVNTtzSUFvTHzsFFmgz4FIR_51Q93V_lpOGA&submissionId=459ea057-5aec-e915-699e-1d9a297aca5a"></a></span>
                        <span><a class="fa fa-envelope" href="mailto:admin@huniarchitectes.com" target="_top"></a></span>
                    </div>

                    <div class="form">
                        <form id="sendContact" method="post" action="#">
                            <?php echo csrf_field(); ?>
                            <div style="overflow:hidden; margin:0 auto; width:600px;">
                                <div class="row_left">
                                    <div class="row">
                                        Nom
                                    </div>

                                    <div class="row">
                                        <input type="text" id="txtName" name="txtName" size="80" class="inputbox txtName" style="width:97%;">
                                        
                                    </div>
                                    <div class="row">
                                        email

                                    </div>
                                    <div class="row">
                                        <input type="text" id="txtEmail" name="txtEmail" size="80" class="inputbox txtEmail" style="width:97%">
                                    </div>
                                    <div class="row">
                                        Objet
                                    </div>
                                    <div class="row">
                                        <input type="text" id="txtSubject" name="txtSubject" size="50" class="inputbox txtSubject" maxlength="200" style="width:97%">
                                    </div>
                                </div>

                                <div class="row_right">
                                    <div class="row">
                                        Message
                                    </div>
                                    <textarea rows="5" id="txtContent" name="txtContent" cols="32" class="inputbox txtarea" style="width:97%;height:80px;"></textarea>
                                </div>
                                <div class="button_1" style="text-align:right;">
                                    <input type="button" name="btnSend" class="btn_gui" id="submitForm" value="Send">
                                    <input type="reset" name="btnReset" class="btn_reset" value="Reset">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--project_box-->
            </Div>


            <script>
                function IsNumberInt(str)
                {
                    for (var i = 0; i < str.length; i++)
                    {
                        var temp = str.substring(i, i + 1);
                        if (!(temp == "," || temp == "." || (temp >= 0 && temp <= 9)))
                        {
                            alert("Chỉ được nhập vào là số");
                            return str.substring(0, i);
                        }
                        if (temp == " " || temp == "," || temp == ".")
                            return str.substring(0, i);
                    }

                    return str;
                }
            </script>
        </div>
        <!--container-->
    </div>
    <!--wrap-->
</Div>

    <!--wraper-->
<div class="thoangbao_mgs">
    <div id="luu_result"></div>
</div>
<div class="popup_photo" id="popup_photo">
    <div id="show_thong_tin"></div>
</div>
<div id="loading_result" style="z-index:999999;"></div>
<div id="khung_bg_view"></div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $('#submitForm').click(function(){
        //e.preventDefault()
        //var frmContactContact = document.getElementById("#sendContact")
        var txtName = $('#txtName').val()
        var txtEmail = $('#txtEmail').val()
        var txtContent=$('#txtContent').val()
        var txtSubject=$('#txtSubject').val()
        if(!txtName)
        {
            alert("Vui lòng nhập họ tên !");
            //frmContactContact.txtName.focus();
            return false;
        }
        
        
        if(!txtEmail || !txtEmail.match(/^([-\d\w][-.\d\w]*)?[-\d\w]@([-\w\d]+\.)+[a-zA-Z]{2,6}$/) )
        {
            alert("Vui lòng nhập đúng định dạng email !");
            //frmContactContact.txtEmail.focus();
            return false;
        }
        if(!txtSubject)
        {
            
            alert("Vui lòng nhập tiêu đề !");
            //frmContactContact.txtSubject.focus();
            return false;
        }
        if(!txtContent)
        {
            alert("Vui lòng nhập nội dung !");
            //frmContactContact.txtContent.focus();
            return false;
        }
        else
        {
            var FormData = $('#sendContact').serialize();
            $.ajax({
                url: '/sendMail',
                type: 'post',
                data: FormData,
                success: function (res){
                    console.log(res)
                    alert('Gửi liên hệ thành công')
                }
            })
        }
        //e.preventDefault()
        return false;
    })
</script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('frontend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>