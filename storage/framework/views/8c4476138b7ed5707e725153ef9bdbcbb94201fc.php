<?php $__env->startSection('meta'); ?>
	<title>Introduction | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="keywords" content="huniarchitectes | huni |architectes" />
    <meta name="description" content="huniarchitectes | huni |architectes" />
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
    <Div class="wraper">
        <div class="wrap">
            <div id="container">
                <style>
                    body{
                        background: url(/uploads/images/Introduction/1(1).jpg) no-repeat center top transparent;
                        -webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                        background-color:#000;
                    }
                </style>
                <div class="main_left">

                    <div class="main_menu">
                        <?php echo $__env->make('frontend.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div><!--main_menu-->
                </div><!--main_left-->
                <Div class="main_right" id="main-right">
                    <div class="box project_box">
                        <?php echo $__env->make('frontend.layouts.logo', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <div class="info_lienhe" style="font-size: 16px; padding-top: 5em !important;"><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

                            <?php echo ($post)? $post->post_content : 'updating...'; ?>


                        </div>
                    </div><!--project_box-->
                </Div>

            </div><!--container-->
        </div><!--wrap-->
    </Div><!--wraper-->
    <div class="thoangbao_mgs" ><div id="luu_result"></div></div> <div class="popup_photo" id="popup_photo" >  <div id="show_thong_tin"></div></div>
    <div id="loading_result" style="z-index:999999;"></div><div id="khung_bg_view" ></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>