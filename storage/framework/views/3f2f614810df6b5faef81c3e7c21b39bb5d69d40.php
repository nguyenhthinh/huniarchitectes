<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?php echo $__env->yieldContent('meta'); ?>
    <link rel="icon" href="<?php echo e(getOption('site', 'favicon')); ?>"/>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55858442-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link href=<?php echo e(asset("js/bootstrap/css/bootstrap.min.css")); ?> rel="stylesheet"/>
    <link href=<?php echo e(asset("css/modern-business.css")); ?> rel="stylesheet"/>

    
    
    

    
        
            
            
            
            
            
            
        
    

    

    
    
    

    
         
    

    
         
    
    
    
    
    
        
        
            
            
                
                
                
            
        
    
    
        
            
            
            
            
        
    
    
        
            
            
            
            
        

        
            
            
            
            
            
            
               
               
            
            
            
            
            
        

        
            
        

        
            
        

        
            
            
            
            
            
        

        
            
            
            
        
    

    
    
    
    
        
            
        
    
    
        
            
        
    

    
        
            
            
            
        

        
            
            
            
            
        

        
        
        
            
            
            
            
            
            
            
            
            
            
            
        

        
        
        
            
            
            
            
        

        
        
        
            
            
            
        

        
        
        
            
            
            
        
    

    <?php echo $__env->yieldContent('styles'); ?>
</head>

<body>
<?php echo $__env->yieldContent('container'); ?>

<?php echo $__env->yieldContent('scripts'); ?>
<script src=<?php echo e(asset("js/jquery/jquery.min.js")); ?>></script>
<script src=<?php echo e(asset("js/bootstrap/js/bootstrap.bundle.min.js")); ?>></script>

</body>

</html>
