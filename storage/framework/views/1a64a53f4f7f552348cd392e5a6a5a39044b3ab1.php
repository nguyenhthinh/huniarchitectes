<?php $__env->startSection('meta'); ?>
	<title>huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="keywords" content="huniarchitectes | huni |architectes" />
    <meta name="description" content="huniarchitectes | huni |architectes" />
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
<div class="wrap" style="position:relative;">
        <div class="language">
            <ul>
                <li><a href="<?php echo e(route('project',['lang'=>'en'])); ?>">ENGLISH</a>|</li>
                <li><a style="margin:0 0 0 1em;" href="<?php echo e(route('project',['lang'=>'fr'])); ?>">FRANCAIS</a></li>
            </ul>
        </div>
        <!--langguage-->
    </div>
    <!--wrap-->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>