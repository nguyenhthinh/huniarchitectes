<?php $__env->startSection('meta'); ?>
	<title>Project | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes" />
    <meta name="keywords" content="huniarchitectes | huni |architectes" />
    <meta name="description" content="huniarchitectes | huni |architectes" />
    <meta name="copyright" content="huniarchitectes | huni |architectes" />
    <meta http-equiv="expires" content="0" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
<Div class="wraper">
    <div class="wrap">
        <div id="container">
            <div class="main_left">

                <div class="main_menu">
                    <?php echo $__env->make('frontend.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <!--main_menu-->
            </div>
            <!--main_left-->
            <Div class="main_right" id="main-right">

                <div class="box project_box">
                    <?php echo $__env->make('frontend.layouts.logo', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php $i=1 ?>
                    <?php $__currentLoopData = $proCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item->cat_name != 'uncategorized'): ?>
                <div class="pro_item pro_<?php echo e($i++); ?>">
                            <div class="background">
                                <div class="background2">
                                    <div class="background3">
                                        <div class="box">
                                            <div class="pro_image">
                                                <a href="<?php echo e(route('project-cat',['lang'=>$lang, 'cat'=> $item->project->alias->alias])); ?>" title="<?php echo e($item->cat_name); ?>"><img src="<?php echo e(showThumb($item->cat_image)); ?>" alt="<?php echo e($item->cat_name); ?>" /></a>
                                            </div>
                                            <div class="pro_name">
                                                <h2><a style="letter-spacing: 0.3em;" href="<?php echo e(route('project-cat',['lang'=>$lang, 'cat'=> $item->project->alias->alias])); ?>"><?php echo e($item->cat_name); ?></a></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <!--pro_item-->
                </div>
                <!--project_box-->
            </div>

        </div>
        <!--container-->
    </div>
    <!--wrap-->
</Div>
<!--wraper-->
<div class="thoangbao_mgs">
    <div id="luu_result"></div>
</div>
<div class="popup_photo" id="popup_photo">
    <div id="show_thong_tin"></div>
</div>
<div id="loading_result" style="z-index:999999;"></div>
<div id="khung_bg_view"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>