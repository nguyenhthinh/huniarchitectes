<?php $__env->startSection('styles'); ?>
    <link rel="stylesheet" href="/admin/custom/dist/css/admin.min.css"/>
    <?php echo $__env->yieldContent('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('container'); ?>
    <div class="container-fluid">
        <div class="row">
            <?php echo $__env->make('administrator.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- End Main Sidebar -->
            <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                <div class="main-navbar sticky-top bg-white">
                    <!-- Main Navbar -->
                    <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
                        <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                            <div id="selectLanguage">
                                Chọn ngôn ngữ data:
                                <a class="<?php echo e((app()->getLocale() == 'en') ? 'active' : ''); ?>" href="<?php echo e(route('changelang', ['lang'=>'en'])); ?>">English</a> |
                                <a class="<?php echo e((app()->getLocale() == 'fr') ? 'active' : ''); ?>" href="<?php echo e(route('changelang', ['lang'=>'fr'])); ?>">Francais</a></div>
                        </form>
                        <ul class="navbar-nav border-left flex-row ">
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    <img class="user-avatar rounded-circle mr-2" src="/admin/images/avatars/0.jpg" alt="User Avatar">
                                    <span class="d-none d-md-inline-block"><?php echo e(auth()->user()->name); ?></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-small">
                                    <a class="dropdown-item" href="<?php echo e(route('admin-change-pass')); ?>">
                                        <i class="material-icons">&#xE7FD;</i> Đổi mật khẩu</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item text-danger" href="<?php echo e(route('admin-logout')); ?>">
                                        <i class="material-icons text-danger">&#xE879;</i> Đăng xuất </a>
                                </div>
                            </li>
                        </ul>
                        <nav class="nav">
                            <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                                <i class="material-icons">&#xE5D2;</i>
                            </a>
                        </nav>
                    </nav>
                </div>
                <!-- / .main-navbar -->
                <div class="main-content-container container-fluid px-4">
                    <?php echo $__env->yieldContent('content'); ?>

                </div>
                <footer class="main-footer d-flex p-2 px-3 bg-white border-top">
                    <span class="copyright ml-auto my-auto mr-2">Copyright © 2019
                      <a href="https://marjobs.com" rel="nofollow">Marjobs</a>
                    </span>
                </footer>
            </main>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>