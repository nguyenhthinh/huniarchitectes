

<?php $__env->startSection('meta'); ?>
    <title>Project | huniarchitectes | huni |architectes</title>
    <meta name="author" content="huniarchitectes | huni |architectes"/>
    <meta name="title" content="<?php echo e(getOption('site','meta_title')); ?>"/>
    <meta name="description" content="<?php echo e(getOption('site','meta_description')); ?>"/>
    <meta name="keywords" content="<?php echo e(getOption('site','keywords')); ?>"/>
    <meta name="copyright" content="huniarchitectes | huni |architectes"/>
    <meta http-equiv="expires" content="0"/>
    <meta name="resource-type" content="document"/>
    <meta name="distribution" content="global"/>
    <meta name="robots" content="index, follow"/>
    <meta name="revisit-after" content="1 days"/>
    <meta name="rating" content="general"/>
    <meta name="google-site-verification" content="AsCbYayW64PolXPOW9tg5vZkOdsHW_uNtsMwWtdcLl8"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark fixed-top">
        <div class="container">
            <?php echo $__env->make('frontend.layouts.logo', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('frontend.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <?php $i=1 ?>
            <?php $__currentLoopData = $proCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($item->cat_name != 'uncategorized'): ?>
                    <div class="col-lg-4 mb-4 no-padding">
                        <div class="pro_item pro_<?php echo e($i++); ?>">
                            <div class="pro_image">
                                <a href="<?php echo e(route('project-cat',['lang'=>$lang, 'cat'=> $item->project->alias->alias])); ?>"
                                   title="<?php echo e($item->cat_name); ?>">
                                    <img
                                            src="<?php echo e(asset(showThumb($item->cat_image))); ?>"
                                            alt="<?php echo e($item->cat_name); ?>"/></a>
                            </div>
                            <div class="pro_name">
                            <h3><?php echo e($item->cat_name); ?>

                            </h3>
                            </div>
                        </div>
                    </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <!--pro_item-->
            <!--project_box-->
        

        
        <!--container-->
        </div>
        <!--wrap-->
    </Div>
    <!--wraper-->
    <div class="thoangbao_mgs">
        <div id="luu_result"></div>
    </div>
    <div class="popup_photo" id="popup_photo">
        <div id="show_thong_tin"></div>
    </div>
    <div id="loading_result" style="z-index:999999;"></div>
    <div id="khung_bg_view"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>