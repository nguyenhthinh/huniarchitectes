<?php

use Illuminate\Database\Seeder;

class DefaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'id' => 1,'username' => 'root','fname'=>'Root', 'lname'=>'Root', 'role'=>1
            ],
            [
                'id' => 2,'username' => 'admin','fname'=>'Admin', 'lname'=>'Admin', 'role'=>2
            ]
        ];

        foreach($user as $u){
            DB::table('users')->insert([
                'id'=> $u['id'],
                'name' => $u['username'],
                'email' => $u['username'].'@example.com',
                'password' => bcrypt('123456'),
            ]);
            DB::table('users_profile')->insert([
                'user_id' => $u['id'],
                'firstname' => $u['fname'],
                'lastname' => $u['lname'],
                'phone' => '',
            ]);
            DB::table('role_user')->insert([
                'user_id'=> $u['id'],
                'role_id' => $u['role']
            ]);
        }

    }
}
