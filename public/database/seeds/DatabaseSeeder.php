<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DefaultRole::class);
        $this->call(DefaultLanguage::class);
        $this->call(DefaultUser::class);
        $this->call(DefaultCategory::class);
    }
}
