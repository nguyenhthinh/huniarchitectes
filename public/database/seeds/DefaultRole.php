<?php

use Illuminate\Database\Seeder;

class DefaultRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id'=> 1,
            'name'=>'root',
            'display_name' => 'Root'
        ]);
        DB::table('roles')->insert([
            'id'=> 2,
            'name'=>'admin',
            'display_name' => 'Admin'
        ]);
        DB::table('roles')->insert([
            'id'=> 3,
            'name'=>'writer',
            'display_name' => 'Writer'
        ]);
    }
}
