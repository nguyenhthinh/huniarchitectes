<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_profile', function(Blueprint $table)
		{
			$table->integer('profile_id', true);
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('firstname', 45)->nullable();
			$table->string('lastname', 45)->nullable();
			$table->dateTime('dateofbirth')->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('avatar', 191)->nullable();
			$table->string('address', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_profile');
	}

}
