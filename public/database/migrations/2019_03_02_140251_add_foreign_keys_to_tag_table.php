<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tag', function(Blueprint $table)
		{
			$table->foreign('post_id', 'tag_ibfk_1')->references('post_id')->on('post')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tag', function(Blueprint $table)
		{
			$table->dropForeign('tag_ibfk_1');
		});
	}

}
