<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category', function(Blueprint $table)
		{
			$table->increments('cat_id');
			$table->string('cat_name', 100)->nullable();
			$table->string('cat_image', 191)->nullable();
			$table->integer('cat_order')->nullable();
			$table->text('cat_description', 65535)->nullable();
			$table->string('cat_meta_title', 191)->nullable();
			$table->string('cat_meta_description', 191)->nullable();
			$table->string('cat_meta_keywords')->nullable();
			$table->integer('cat_lang')->unsigned()->nullable()->index('cat_lang');
			$table->integer('cat_parent')->unsigned()->nullable();
			$table->string('cat_type', 10)->nullable()->default('post');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category');
	}

}
