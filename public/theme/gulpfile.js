// Include gulp
var gulp = require('gulp');
var browserSync = require('browser-sync').create();

// Include Our Plugins
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCss = require('gulp-cssnano');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var minify = require('gulp-minify');

gulp.task('sass', function() {
    gulp.src([
        './src/css/*.scss',
    ])
        .pipe(plumber())
        .pipe(sass.sync({ // Have to use sass.sync - See Issue (https://github.com/dlmanning/gulp-sass/issues/90)
            outputStyle: 'compressed',
            errLogToConsole: true
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('babel', function () {
    gulp.src('./src/js/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(minify())
        //.pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/js/'))
});

// Watch Files For Changes
gulp.task('watch', function () {
    browserSync.init({
        server: "./"
    });

    gulp.watch("./src/css/*.scss", ['sass']).on('error', swallowError);
    gulp.watch("./src/js/**/*.js", ['babel']).on('error', swallowError);
    gulp.watch("./demo/*.*").on('change',browserSync.reload);
    gulp.watch("./src/**/*.*").on('change',browserSync.reload);
});

// Default Task
gulp.task('default', ['sass', 'babel', 'watch']);


function swallowError (error) {
    // If you want details of the error in the console
    console.log(error.toString())
    this.emit('end')
}