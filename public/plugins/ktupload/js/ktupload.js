"use strict";

var KTUItemID = 0;
var KTUElement;
var KTUElementItem;
var KTUElementAddItem;
jQuery.fn.extend({
  KTUInputUpload: function KTUInputUpload() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    console.log('SET VARIABLE');
    console.log(config);
    var cf_curent_items = config.curent_item ? config.curent_item : "";
    var cf_upload_url = config.upload_url ? config.upload_url : 'upload.php';
    var cf_edit_text = config.edit_text ? config.edit_text : 'CHOOSE IMAGE';
    var cf_loading_icon = config.loading_icon ? config.loading_icon : '/../dist/img/loadingsvg.svg';
    var cf_thumb_func = config.thumb_func ? config.thumb_func : null;
    console.log('SET TEMPLATE');
    var ktu_id = this[0].attributes.id.nodeValue;
    var ktu_el = '#' + this[0].attributes.id.nodeValue;
    var ktu_wrapper = "<div class=\"ktu-input-wrapper\">{KTU_CONTENT}</div>";
    var ktu_upload_item = "<div class=\"ktu-item\" id=\"ktu-item-{ID}\">\n                                    <span class=\"ktu-select-btn\" onclick=\"KTUChooseImage('{ID}')\" id=\"ktu-add-btn-{ID}\">".concat(cf_edit_text, "</span>\n                                    <div class=\"ktu-thumb\"><img id=\"ktu-thumb-{ID}\" src=\"{IMG_SRC}\"/></div>\n                                    <input type=\"file\" onchange=\"KTUSelectImage('{ID}', '").concat(cf_upload_url, "', true)\" class=\"ktu-file-input ktu-hide\" id=\"ktu-file-{ID}\"/>\n                                    <div class=\"ktu-loading\" id=\"ktu-loading-{ID}\"><img src=\"").concat(cf_loading_icon, "\"/></div>\n                                </div>"); //RENDER CURENT ITEMS

    console.log('RENDER CURENT ITEMS');
    var render_item = '';
    render_item = ktu_upload_item.replace(/{ID}/g, ktu_id);
    render_item = render_item.replace(/{IMG_SRC}/g, cf_curent_items);

    if (cf_curent_items != '') {
      render_item = render_item.replace(/{IMG_SRC}/g, cf_curent_items);
    } // render_curent_items += "{KTU_CONTENT}"


    var html = ktu_wrapper.replace(/{KTU_CONTENT}/g, render_item);
    $('#' + ktu_id).hide();
    $(ktu_el).after(html);
  },
  KTUGalleryUpload: function KTUGalleryUpload() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    console.log('SET VARIABLE');
    console.log(config);
    var cf_curent_items = config.curent_item ? config.curent_item : [];
    var cf_upload_url = config.upload_url ? config.upload_url : 'upload.php';
    var cf_add_text = config.add_text ? config.add_text : 'ADD IMAGE';
    var cf_edit_text = config.edit_text ? config.edit_text : 'CHOOSE IMAGE';
    var cf_loading_icon = config.loading_icon ? config.loading_icon : '/../dist/img/loadingsvg.svg';
    var cf_add_icon = config.add_icon ? config.add_icon : '/../dist/img/add-photo.png';
    var cf_image_input_name = config.image_input_name ? config.image_input_name : 'image';
    var start_item = cf_curent_items.length;
    var cf_thumb_func = config.thumb_func ? config.thumb_func : null;
    var i;
    console.log('SET TEMPLATE');
    var ktu_el = '#' + this[0].attributes.id.nodeValue;
    var ktu_wrapper = "<div class=\"ktu-wrapper\">{KTU_CONTENT}</div>";
    var ktu_upload_item = "<div class=\"ktu-item\" id=\"ktu-item-{ID}\">\n                                    <span class=\"ktu-select-btn\" onclick=\"KTUChooseImage({ID})\" id=\"ktu-add-btn-{ID}\">".concat(cf_edit_text, "</span>\n                                    <div class=\"ktu-thumb\"><img class=\"{IMG_CLASS}\" id=\"ktu-thumb-{ID}\" src=\"{IMG_SRC}\"/></div>\n                                    <span class=\"ktu-remove-btn {REMOVE_CLASS}\" id=\"ktu-remove-btn-{ID}\" onclick=\"KTUDeleteItem({ID})\">x</span>\n                                    <input type=\"file\"  onchange=\"KTUSelectImage({ID}, '").concat(cf_upload_url, "')\" class=\"ktu-file-input ktu-hide\" id=\"ktu-file-{ID}\"/>\n                                    <input type=\"hidden\" name=\"").concat(cf_image_input_name, "[]\" value=\"{FILE_SRC}\" id=\"ktu-image-{ID}\"/>\n                                    <div class=\"ktu-loading\" id=\"ktu-loading-{ID}\"><img src=\"").concat(cf_loading_icon, "\"/></div>\n                                </div>");
    var ktu_add_item = "<div class=\"ktu-item\" id=\"ktu-add-item\">\n                                    <span class=\"ktu-select-btn\" onclick=\"KTUAddItem()\">".concat(cf_add_text, "</span>\n                                    <div class=\"ktu-thumb\"><img src=\"").concat(cf_add_icon, "\"/></div>\n                                </div>");
    KTUItemID = start_item + 1;
    KTUElement = ktu_el;
    KTUElementItem = ktu_upload_item;
    KTUElementAddItem = ktu_add_item; //RENDER CURENT ITEMS

    console.log('RENDER CURENT ITEMS');
    var render_curent_items = '';
    console.log(start_item);

    for (i = 0; i < start_item; i++) {
      console.log('RENDER CURENT ITEMS' + i);
      render_curent_items += KTURenderItem(i, cf_curent_items[i], cf_thumb_func);
    } // render_curent_items += "{KTU_CONTENT}"


    var html = ktu_wrapper.replace(/{KTU_CONTENT}/g, render_curent_items);
    $(ktu_el).html(html);
    $(ktu_el).find('.ktu-wrapper').append(ktu_add_item);
  }
});

function KTURenderItem(i) {
  var image = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var thumbFunc = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  //console.log(image, image !== null)
  var ktu_upload_item_html = KTUElementItem;
  var add_class = "ktu-show";
  var edit_class = "ktu-hide";
  var img_class = "ktu-hide";
  var remove_class = "ktu-show";

  if (image !== null) {
    add_class = "ktu-hide";
    edit_class = "ktu-show";
    img_class = "ktu-show";
    remove_class = "ktu-show";
  }

  ktu_upload_item_html = ktu_upload_item_html.replace(/{ID}/g, i);

  if (thumbFunc) {
    var imageURL = window[thumbFunc](image);
    ktu_upload_item_html = ktu_upload_item_html.replace(/{IMG_SRC}/g, imageURL);
  } else {
    ktu_upload_item_html = ktu_upload_item_html.replace(/{IMG_SRC}/g, image);
  }

  ktu_upload_item_html = ktu_upload_item_html.replace(/{ADD_CLASS}/g, add_class);
  ktu_upload_item_html = ktu_upload_item_html.replace(/{EDIT_CLASS}/g, edit_class);
  ktu_upload_item_html = ktu_upload_item_html.replace(/{IMG_CLASS}/g, img_class);
  ktu_upload_item_html = ktu_upload_item_html.replace(/{REMOVE_CLASS}/g, remove_class);
  return ktu_upload_item_html;
}

function loading(i) {
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  if (state) $('#ktu-loading-' + i).show();else $('#ktu-loading-' + i).hide();
}

function KTUChooseImage(i) {
  $('#ktu-file-' + i).trigger('click');
}

function KTUSelectImage(i, url) {
  var input = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  // console.log("KTUSelectImage")
  // console.log(i)
  loading(i); // let file = $('#ktu-file-'+i).files

  var formData = new FormData(); // let props = $('#ktu-file-'+i).prop('files')

  var file = $('#ktu-file-' + i).prop('files')[0]; //props[0];
  // console.log(file)

  formData.append('file', file);
  $.ajax({
    url: url,
    data: formData,
    type: 'POST',
    cache: false,
    contentType: false,
    processData: false
  }).then(function (res) {
    //console.log(res)
    loading(i, false);

    if (!input) {
      $('#ktu-remove-btn-' + i).show();
      $('#ktu-thumb-' + i).attr("src", res.thumb);
      $('#ktu-thumb-' + i).show();
      $('#ktu-image-' + i).val(res.file);
    } else {
      $('#ktu-thumb-' + i).attr("src", res.thumb);
      $('#ktu-thumb-' + i).show();
      $('#' + i).val(res.file);
    }
  }).catch(function (e) {
    alert(e.responseText);
  });
}

function KTUDeleteItem(i) {
  $('#ktu-item-' + i).remove();
}

function KTUAddItem() {
  $('#ktu-add-item').remove();
  var html = KTURenderItem(KTUItemID);
  KTUItemID += 1;
  $(KTUElement).find('.ktu-wrapper').append(html);
  $(KTUElement).find('.ktu-wrapper').append(KTUElementAddItem);
}