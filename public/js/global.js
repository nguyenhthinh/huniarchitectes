var token = localStorage.getItem('token');
if(token){
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer '+ token);
        }
    });
}

function getFilePath(type, path, size = 'thumb'){
    var base = '/';
    //dd($type, $x, $ext, $extArray);
    if(type == 'image'){
        return base+'_uploads/images/'+size+'/'+path;
    }else{
        return base+path;
    }
}

function srcAvatar(str){
    if(str){
        return getFilePath('image', str);
    }else{
        return "/theme/assets/image-resources/gravatar.jpg"
    }
}

function logout(){
    $.ajax({
        url: '/api/logout',
    }).done(function(res){
        localStorage.clear();
        window.location.href = '/'
    }).fail(function(){
        alert('Some thing were wrong!')
    })
}

function pagination(el, data, fnc){
    var htmlTemplate = '<ul class="pagination">' +
        '<li class="page-item" onclick="'+fnc+'(\'{FIRST_URL}\')"><a class="page-link" href="javascript:void(0)">ĐẦU</a></li>' +
        '<li class="page-item" onclick="'+fnc+'(\'{PREV_URL}\')"><a class="page-link" href="javascript:void(0)">TRƯỚC</a></li>' +
        '<li class="page-item" onclick="'+fnc+'(\'{NEXT_URL}\')"><a class="page-link" href="javascript:void(0)">SAU</a></li>' +
        '<li class="page-item" onclick="'+fnc+'(\'{LAST_URL}\')"><a class="page-link" href="javascript:void(0)">CUỐI</a></li>' +
        '</ul>';
    var html = htmlTemplate.replace(/{FIRST_URL}/,data.first_page_url)
    html = html.replace(/{PREV_URL}/,data.prev_page_url)
    html = html.replace(/{NEXT_URL}/,data.next_page_url)
    html = html.replace(/{NEXT_URL}/,data.last_page_url)
    $(el).html(html)
}

$(document).ready(function() {
    $("time.timeago").timeago();
});