function selectSkill(el){
    var $select = $(el).selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        maxItems: null,
        valueField: 'skill_id',
        labelField: 'name',
        searchField: 'name',
        options: [],
        create: false,
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/api/skill',
                type: 'GET',
                data: {
                    query: query,
                    page_limit: 10
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data);
                }
            });
        }

    });
}
function selectEmplevel(el){
    var $select = $(el).selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        maxItems: 1,
        valueField: 'level_id',
        labelField: 'name',
        searchField: 'name',
        options: [],
        create: false,
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/api/emplv',
                type: 'GET',
                data: {
                    query: query,
                    page_limit: 10
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data);
                }
            });
        }
    });
}
function selectRole(el){
    var $select = $(el).selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        maxItems: null,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        options: [],
        create: false,
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/api/role',
                type: 'GET',
                data: {
                    query: query,
                    page_limit: 10
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data);
                }
            });
        }
    });
}

function listUser(){
    type = localStorage.getItem('viewStyle');
    if(!type || type == 'grid'){
        loadUserGrid()
    }else{
        loadUserTable()
    }
}

function loadUserTable(){
    $('#datatable-responsive').DataTable( {
        responsive: true,
        ajax: {
            url: API_ALL_USER,
            dataSrc: 'data'
        },
        columns: [
            {data: 'id'},
            {data: 'user_profiles.firstname'},
            {data: 'user_profiles.lastname'},
            {data: 'email'},
            {data: 'email'},
            {data: 'email'},
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var buttonID = full.id;
                    var template = '<button onclick="viewUser({ID})" class="btn btn-sm btn-info" role="button">Chi tiết</button>\
                                    <button onclick="editUser({ID})" class="btn btn-sm btn-primary" role="button">Chỉnh sửa</button>\
                                    <button onclick="deleteUser({ID})" class="btn btn-sm btn-warning" role="button">Xóa</button>';
                    return template.replace(/{ID}/g, buttonID)
                }
            },
        ]
    } );
}

function loadUserGrid(){
    var formData = $('#filter-form').serialize()
    $.ajax({
        url: API_ALL_USER,
        data: formData
    }).done(function(data){
        renderGridUser(data)
    })
}

function loadPagiUser(url){
    $.ajax({
        url: url,
        data: {}
    }).done(function(data){
        renderGridUser(data)
    })
}

function renderGridUser(data){
    var htmlTemplate = `<div class="col-xs-12 col-sm-4 col-md-2">
                                <div class="panel-layout hover-shadow">
                                    <div class="panel-box">
                                        <div class="panel-content bg-white">
                                            <div class="">
                                                <img src="{AVATAR}" alt="" class="img-bordered img-circle mrg10B">
                                                <h5>{NAME}</h5>
                                                <span>@{USERNAME}</span><hr/>                                                
                                                <span class="opacity-60">{EMAIL}</span>
                                            </div>
                                        </div>
                                        <div class="panel-content pad15A bg-white">
                                            <div class="center-vertical">
                                                <ul class="center-content list-group list-group-separator row mrg0A">
                                                    <li class="col-md-6">
                                                        <a class="btn btn-sm btn-info btn-block" href="#" title="">
                                                            <i class="glyph-icon icon-edit"></i>
                                                            Cập nhật
                                                        </a>
                                                    </li>
                                                    <li class="col-md-6">
                                                        <a class="btn btn-sm btn-warning btn-block" href="#" title="">
                                                            <i class="glyph-icon icon-trash-o"></i>
                                                            Xóa
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    var renderStr = `<div class="col-xs-12 col-sm-4 col-md-2">
                            <div class="panel-layout hover-shadow" onclick="addUser()">                                
                                <div class="panel-box">
                                    <div class="panel-content bg-white">
                                        <div class="">
                                            <br/><br/>
                                            <img src="/theme/assets/images/newProject.jpg" alt="" class="img-circle mrg10B">
                                            <h5>THÊM THÀNH VIÊN</h5>
                                            <hr/>
                                            <br/><br/><br/><br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;
    $.each(data.data.data, function(key, item){
        //console.log(item);
        var html = htmlTemplate
        html = html.replace(/{AVATAR}/g, srcAvatar(item.avatar))
        html = html.replace(/{USERNAME}/g, item.name)
        html = html.replace(/{EMAIL}/g, item.email)
        html = html.replace(/{NAME}/g, item.user_profiles.firstname+' '+item.user_profiles.lastname)
        renderStr += html
    })
    $('#listGridUser').html(renderStr)
    pagination('#pagination', data.data, 'loadPagiUser')
}

function addUser(){
    selectSkill('#selectSkill')
    selectEmplevel('#selectEmpLevel')
    selectRole('#selectRole')
    $('#newUserPanel').modal('show')
}

function createUser(){
    $('.form-error').html("");
    var formData = $('#addUserForm').serialize()
    $.ajax({
        url: '/api/user/create',
        type:'GET',
        data: formData
    }).done(function (data) {
        listUser()
        $('#newUserPanel').modal('hide')
        $('#addUserForm').trigger("reset");
    }).fail(function(e, data){
        console.log(e.responseJSON);
        $.each(e.responseJSON.errors, function(key, errors){
            str = '';
            $.each(errors, function (key2, error){
                str += (' '+error)
            })
            $('#error-'+key).text(str);
        })
    })
}

function viewUser(id){
    console.log("VIEW USER")
}
function editUser(id){
    console.log("EDIT USER")
}
function deleteUser(id){
    console.log("DELETE USER")
}