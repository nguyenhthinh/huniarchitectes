function renderActivity(data){
    var render = ""
    var htmlTemplateUser = `<li class="activity" onclick="goAction()">
                                <img class="img-circle" src="{OBJ_AVATAR}" alt="{OBJ_NAME}" title="{OBJ_NAME}">
                                <b>{OBJ_NAME}</b> {ACTION_CONTENT} <b>{TARGET_NAME}</b> 
                                - <time class="timeago" datetime="{ACTION_TIME}"></time>
                            </li>`
    var htmlTemplateSystem = `<li class="activity" onclick="goAction()">
                                <b>{OBJ_NAME}</b> {ACTION_CONTENT} 
                                - <time class="timeago" datetime="{ACTION_TIME}"></time>
                            </li>`
    $.each(data, function (key, item){
        if(item.type == "USER"){
            var html = htmlTemplateUser
            html = html.replace(/{PROJECT_COLOR}/g, item.project_color)
            html = html.replace(/{PROJECT_NAME}/g, item.project_name)
            html = html.replace(/{OBJ_AVATAR}/g, srcAvatar(item.obj.image))
            html = html.replace(/{OBJ_NAME}/g, item.obj.name)
            html = html.replace(/{ACTION_CONTENT}/g, item.action.content)
            html = html.replace(/{ACTION_TIME}/g, item.action.time)
            html = html.replace(/{TARGET_NAME}/g, item.target.name)
        }else{
            var html = htmlTemplateSystem
            html = html.replace(/{PROJECT_COLOR}/g, item.project_color)
            html = html.replace(/{PROJECT_NAME}/g, item.project_name)
            html = html.replace(/{OBJ_NAME}/g, item.obj.name)
            html = html.replace(/{ACTION_CONTENT}/g, item.action.content)
            html = html.replace(/{ACTION_TIME}/g, item.action.time)
        }
        render += html
    })
    return render;
}

function goAction(){}

function loadProjectActivity(){
    var data = [
        {
            id: 1,
            project_id: 1,
            type: 'USER',
            project_name: "Test project",
            project_color: "blue",
            obj:{
                id: 1,
                name: "Tran Cong Thai",
                image: null
            },
            target: {
                id: 1,
                name: "#32 Task test thử",
                image: null
            },
            action: {
                type: "ADDTASK",
                content: "đã thêm task",
                time: "2019-01-10 08:00:00"
            }
        },
        {
            id: 1,
            type: 'SYSTEM',
            project_id: 1,
            project_name: "Test project 2",
            project_color: "red",
            obj:{
                id: 1,
                name: "#32 Task test thử",
                image: null
            },
            target: null,
            action: {
                type: "OVERDUE",
                content: "đã overdue",
                time: "2019-01-10 08:00:00"
            }
        }
    ]
    $('#projectActivity').html(renderActivity(data))
    $("time.timeago").timeago();
}

function loadProjectChartOverview(){
    var dataSet = [
        { label: "Đang thực hiện", data: 100, color: getUIColor('info') },
        { label: "Đang QC", data: 80, color: getUIColor('warning') },
        { label: "Trễ tiến độ", data: 20, color: getUIColor('danger') },
        { label: "Đã hoàn thành", data: 90, color: getUIColor('primary') },
        { label: "Đã đóng", data: 40, color: getUIColor('success') }
    ];

    $.plot('#overviewChart', dataSet, {
        series: {
            pie: {
                show: true
            },
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s"
        },
        grid: {
            hoverable: true,
            clickable: true
        }
    });
}