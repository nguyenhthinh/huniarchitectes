function renderListMember(data){
    var render = "";
    var htmlTemplate = `<li class="status-badge mrg10A">
                        <img class="img-circle member_image" width="40" src="{MEMBER_AVATAR}" alt="{MEMBER_NAME}" title="{MEMBER_NAME}">
                </li>`
    $.each(data, function(key, item){
            var html = htmlTemplate
            html = html.replace(/{MEMBER_NAME}/g,(item.user.user_profiles.firstname+' '+item.user.user_profiles.lastname))
            html = html.replace(/{MEMBER_AVATAR}/g,srcAvatar(item.user.user_profiles.avatar))
            render += html
    })
    return render;
}

function renderProjects(data){
    var render = `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="panel-layout project-grid-item hover-shadow" onclick="newProject()">                                
                                <div class="panel-box">
                                    <div class="panel-content bg-white">
                                        <div class="">
                                            <div class="project_image">
                                                <img src="/theme/assets/images/newProject.jpg" alt="" class="img-circle mrg10B">
                                            </div>
                                            <h5>Thêm dự án</h5>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>`
    var htmlTemplate = `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <span class="activity-bagde">3</span>
                            <div class="panel-layout project-grid-item hover-shadow" onclick="goDetail({PROJECT_ID})">                                
                                <div class="panel-box">
                                    <div class="panel-content bg-{PROJECT_COLOR}">
                                        <div class="">
                                            <div class="project_image">
                                                <img src="{PROJECT_IMAGE}">
                                            </div>
                                            <h5>{PROJECT_NAME}</h5>
                                        </div>
                                    </div>
                                    <div class="panel-content pad15A bg-white">
                                        <div class="center-vertical">
                                            <ul class="center-content list-group list-group-separator row mrg0A list-member">
                                            {PROJECT_MEMBERS}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`
    console.log(data);
    $.each(data.data, function(key, item){
        console.log(item)
        var html = htmlTemplate
        html = html.replace(/{PROJECT_ID}/g,item.project_id)
        html = html.replace(/{PROJECT_NAME}/g,item.name)
        html = html.replace(/{PROJECT_IMAGE}/g,srcAvatar(item.image))
        html = html.replace(/{PROJECT_COLOR}/g,(item.color && item.color.code)?item.color.code:'white')
        html = html.replace(/{PROJECT_MEMBERS}/g,renderListMember(item.project_members_distinct))
        render += html
    })
    $('#listProject').html(render);
}

function renderActivity(data){
    var render = ""
    var htmlTemplateUser = `<li class="activity" onclick="goAction()">
                                <span class="bs-label bg-{PROJECT_COLOR}">{PROJECT_NAME}</span>
                                <img class="img-circle" src="{OBJ_AVATAR}" alt="{OBJ_NAME}" title="{OBJ_NAME}">
                                <b>{OBJ_NAME}</b> {ACTION_CONTENT} <b>{TARGET_NAME}</b> 
                                - <time class="timeago" datetime="{ACTION_TIME}"></time>
                            </li>`
    var htmlTemplateSystem = `<li class="activity" onclick="goAction()">
                                <span class="bs-label bg-{PROJECT_COLOR}">{PROJECT_NAME}</span>
                                <b>{OBJ_NAME}</b> {ACTION_CONTENT} 
                                - <time class="timeago" datetime="{ACTION_TIME}"></time>
                            </li>`
    $.each(data, function (key, item){
        if(item.type == "USER"){
            var html = htmlTemplateUser
            html = html.replace(/{PROJECT_COLOR}/g, item.project_color)
            html = html.replace(/{PROJECT_NAME}/g, item.project_name)
            html = html.replace(/{OBJ_AVATAR}/g, srcAvatar(item.obj.image))
            html = html.replace(/{OBJ_NAME}/g, item.obj.name)
            html = html.replace(/{ACTION_CONTENT}/g, item.action.content)
            html = html.replace(/{ACTION_TIME}/g, item.action.time)
            html = html.replace(/{TARGET_NAME}/g, item.target.name)
        }else{
            var html = htmlTemplateSystem
            html = html.replace(/{PROJECT_COLOR}/g, item.project_color)
            html = html.replace(/{PROJECT_NAME}/g, item.project_name)
            html = html.replace(/{OBJ_NAME}/g, item.obj.name)
            html = html.replace(/{ACTION_CONTENT}/g, item.action.content)
            html = html.replace(/{ACTION_TIME}/g, item.action.time)
        }
        render += html
    })
    return render;
}

function goDetail(proID){
    window.location.href = '/projects/detail/'+proID;
}

function goAction(){}

function loadProject(){
    $.ajax({
        url: '/api/project'
    }).done(function (data){
        renderProjects(data.data);
    }).fail(function(e, data){
        alert(e.statusText);
    })

}

function loadProjectActivity(){
    var data = [
        {
            id: 1,
            project_id: 1,
            type: 'USER',
            project_name: "Test project",
            project_color: "blue",
            obj:{
                id: 1,
                name: "Tran Cong Thai",
                image: null
            },
            target: {
                id: 1,
                name: "#32 Task test thử",
                image: null
            },
            action: {
                type: "ADDTASK",
                content: "đã thêm task",
                time: "2019-01-10 08:00:00"
            }
        },
        {
            id: 1,
            type: 'SYSTEM',
            project_id: 1,
            project_name: "Test project 2",
            project_color: "red",
            obj:{
                id: 1,
                name: "#32 Task test thử",
                image: null
            },
            target: null,
            action: {
                type: "OVERDUE",
                content: "đã overdue",
                time: "2019-01-10 08:00:00"
            }
        }
    ]
    $('#projectActivity').html(renderActivity(data))
    $("time.timeago").timeago();
}

function newProject(){
    selectUser('#selectLeader', 1)
    selectUser('#selectUser')
    $('#newProjectPanel').modal('show')
}
function selectUser(el, max = null){
    var $select = $(el).selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        maxItems: max,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        options: [],
        create: false,
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/api/user',
                type: 'GET',
                data: {
                    name: query,
                    pagination: 10
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data.data);
                }
            });
        }
    });
}

function createProject(){
    $('.form-error').html("");
    var formData = new FormData(document.getElementById('addProjectForm'));
    $.ajax({
        url: '/api/project',
        type:'POST',
        data: formData,
        contentType: false,
        processData: false
    }).done(function (data) {
        loadProject()
        $('#newProjectPanel').modal('hide')
        $('#addProjectForm').trigger("reset");
    }).fail(function(e, data){
        console.log(e.responseJSON);
        $.each(e.responseJSON.errors, function(key, errors){
            str = '';
            $.each(errors, function (key2, error){
                str += (' '+error)
            })
            $('#error-'+key).text(str);
        })
    })
}