function login(){
    $('#loadingSpin').show();
    var formData = $("#loginForm").serialize()
    $.ajax({
        url: API_LOGIN,
        data: formData,
        type: "post"
    }).done(function (res){
        $('#loadingSpin').hide()
        $('#formAlert').html('<div class="alert alert-success">Đăng nhập thành công</div>')
        localStorage.setItem('token', res.token)
        setTimeout(function(){
            window.location.href = LOGIN_SUCCESS;
        })

    }).fail(function() {
        $('#loadingSpin').hide()
        $('#formAlert').html('<div class="alert alert-danger">Đăng nhập thất bại</div>')
    })
    // setTimeout(function(){
    //     $('#loadingSpin').hide();
    // }, 1000)
}