"use strict";

$(document).ready(function () {
  $('#loginForm').submit(function (e) {
    var formData = $(this).serialize();
    console.log(formData);
    $.ajax({
      url: API_LOGIN,
      data: formData,
      type: "POST"
    }).then(function (res) {
      $.toast({
        heading: 'Đăng nhập thành công',
        text: res.message,
        hideAfter: 1000,
        icon: 'success',
        afterHidden: function afterHidden() {
          window.location.href = URL_HOME;
        }
      });
    }).catch(function (e) {
      console.log(e); //alert(e.responseText)

      $.toast({
        heading: 'Error',
        text: e.responseJSON.message,
        showHideTransition: 'fade',
        icon: 'error',
        hideAfter: false
      });
    });
    e.preventDefault();
  });
});