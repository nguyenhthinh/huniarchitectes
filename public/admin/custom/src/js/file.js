function getImage(url, size = 'thumb'){
    //check has 'uploads/'
    let res = url.match(/uploads\//g);
    if(res) return url;
    else{
        let ext = ['jpg', 'png', 'gif', 'jpeg']
        for(let i = 0; i < ext.length ; i++){
            let regex = new RegExp( ext[i], 'g' );
            //console.log(url.match(regex));
            if(url.match(regex)) {
                return '/uploads/images/'+size+'/'+url;
            }
        }
    }
}