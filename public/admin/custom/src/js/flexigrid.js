var flexigridSelected = [];
var flexigridState = 'idle'; //idle | editing | adding | loading
var flexigridPage = 1;
var flexigridPer_page = 15;
var flexigridCols = [];
var flexigridUrl = '';
var flexigridUrlEdit = '';
var flexigridUrlAdd = '';
var flexigridUrlDelete = '';
jQuery.fn.extend({
    makeFlexiGrid: function (
        config = {},
        page = 1,
        per_page = 15
    ) {
        flexigridSelected = [];
        flexigridPage = page;
        flexigridPer_page = per_page;
        flexigridCols = config.cols;
        flexigridUrl = (config.url) ? config.url : '';
        flexigridUrlEdit = (config.url_update) ? config.url_update : '';
        flexigridUrlAdd = (config.url_add) ? config.url_add : '';
        flexigridUrlDelete = (config.url_delete) ? config.url_delete : '';
        //console.log(config.cols)

        var el = '#'+this[0].attributes.id.nodeValue;

        var renderContent = '';



        renderContent += "<ul class=\"table-header-button\" style='display: none'>{HEADER}</ul>"

        var tableHeading = '<form id="dataContent"><table class="table table-hover table-striped table-load-setting"><thead><tr>';
        $.each(config.cols, function(key, item){
            if(key >0) tableHeading += '<th>'+item.name+'</th>'
        })
        tableHeading += '</tr></thead>'
        renderContent += tableHeading


        var TBODY =  '<tbody></tbody>'

        renderContent += TBODY
        renderContent += '</table></form>'

        renderContent += `<div class="hiddenButtonPanel">
                                <button class="btn btn-success" onclick="updateRows()">Save</button>
                                <button class="btn btn-danger" onclick="closeEditRows()">Cancel</button>
                          </div>`
        renderContent += "<div class=\"table-footer-pagination\" style='display: none'>{FOOTER}</div>"


        $(el).show()
        $(el).html(renderContent)
        $(el).append('<div class="loading-content" id="loading-content">Loading ... </div>')

        $.ajax({
            url: config.url,
            type: 'POST',
            data: {page: page, per_page: per_page},
            dataType: 'json'
        }).then((response)=>{
            var _FOOTER = `
                        <ul>
                            ${renderPagination(response)}
                        </ul>
                    `

            var _HEADER = `
                        <li>
                            <a href="javascript:addModal()">
                                <img src="/assets/pems/icons/plus.png" height="14">
                                <span>Add</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:editRows()">
                                <img src="/assets/pems/icons/edit.png" height="14">
                                <span>Edit</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:delRows()">
                                <img src="/assets/pems/icons/trash.png" height="14">
                                <span>Delete</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:exportRows()">
                                <img src="/assets/pems/icons/excel.png" height="14">
                                <span>Export to Excel</span>
                            </a>
                        </li>
                    `
            //console.log(renderItemFlexGrid(response, config.cols));
            renderContent = renderContent.replace(/{HEADER}/g, _HEADER)
            renderContent = renderContent.replace(/{FOOTER}/g, _FOOTER)
            renderContent = renderContent.replace(TBODY, renderItemFlexGrid(response.data, config.cols))

            $(el).html(renderContent)

            $('.table-footer-pagination').show()
            $('.table-header-button').show()
            //$(el).append('<div class="loading-content">Loading ... </div>')
            //$('#loading-content').remove()
        }).catch((e)=>{
            $('#loading-content').text('Something went wrong')
        })

    }
});

function renderItemFlexGrid(data, cols){
    var html = ''
    $.each(data, function(key, item){
        var itemHtml = '<tr id="{ID}" data-id="{DID}" onclick="{SELECT_FUNC}">'
        var _ID = 0;
        $.each(cols, function(k, val){
            if(k == 0){
                console.log(k)
                _ID = item[val.id];
            }else{
                //console.log(val, item[val.id]);
                itemHtml += `<td>
                            <span class="showLabel" id="label-${val.id}-{DID}">${item[val.id]}</span>`
                if(val.id == 'load_status' ){
                    let selectedON = (item[val.id]) ? 'selected': '';
                    let selectedOFF = (item[val.id]) ? '': 'selected';
                    itemHtml += `<select class="hiddenInput" id="input-${val.id}-{DID}">
                                <option value="1" ${selectedON}>ON</option>
                                <option value="0" ${selectedOFF}>OFF</option>
                             </select>`
                }
                else{
                    itemHtml += `<input class="hiddenInput" id="input-${val.id}-{DID}" value="${item[val.id]}"/>`
                }

                itemHtml += `
                            <input type="hidden" id="ov-input-${val.id}-{DID}" value="${item[val.id]}"/>
                            <div class="hiddenError" id="error-${val.id}-{DID}"></div>
                         </td>`
                //console.log(k, (cols.length -1))
            }
        })
        itemHtml = itemHtml.replace(/{ID}/g, 'settingRow-'+_ID)
        itemHtml = itemHtml.replace(/{DID}/g, _ID)
        itemHtml = itemHtml.replace(/{SELECT_FUNC}/g, 'selectRowFlexGrid(\'settingRow-'+_ID+'\')')
        itemHtml += '</tr>'
        html += itemHtml
    })
    return html;
}

function selectRowFlexGrid(id){
    if(flexigridState == 'idle'){
        let allow = false;
        $.each($('#'+id+' input'), function(k, item){
            if($(item).is(':focus')) allow = true;
        })
        if(!allow){
            var did = $('#'+id).data('id')
            if(!$('#'+id).hasClass('selected')){
                $('#'+id).addClass('selected')
                flexigridSelected.push(did)
            }
            else{
                $('#'+id).removeClass('selected')
                // flexigridSelected.push($('#'+id).data('id'))
                flexigridSelected = flexigridSelected.filter(function(value, index, arr){
                    return (did != value)
                    // return true;
                });
            }
        }else {
            alert('Complete your edit or cancel before do it')
        }

    }
}

function addModal(){
    $('#addModal').modal('show')
}

function editRows(){
    if(flexigridState == 'idle'){
        $.each(flexigridSelected, function (key, id){
            $(`#settingRow-${id} .hiddenInput, #settingRow-${id} .hiddenError`).show();
            $(`#settingRow-${id} .showLabel`).hide();
        })
        $('.hiddenButtonPanel').show();
        flexigridState = 'editing'
    }
}

function closeEditRows(){
    $.each(flexigridSelected, function (key, id){
        $(`#settingRow-${id} .hiddenInput, #settingRow-${id} .hiddenError`).hide();
        $(`#settingRow-${id} .showLabel`).show();
    })
    $('.hiddenButtonPanel').hide();
    flexigridState = 'idle'
}

function updateRows(prefixID = ''){
    var formData = new FormData();
    $.each(flexigridSelected, function (key, id){
        //input-load_no-settingRow-1
        //console.log(id);
        $.each(flexigridCols, function(k, val){

            $(`#input-${val.id}-${prefixID}${id}`).val()
            //console.log(`#input-${val.id}-${prefixID}${id}`, value);
            formData.append(val.id+'-'+id, value);
        })
        formData.append('item-id-'+id,id)
    })
    formData.append('ids', flexigridSelected)
    //console.log(formData)
    $.ajax({
        url: flexigridUrlEdit,
        data: formData,
        type: 'post',
        processData: false,
        contentType: false,
    }).then((response)=>{
        $.each(flexigridSelected, function (key, id){
            $.each(flexigridCols, function(k, val){
                console.log(response.data[id][val.id]);
                // $(`#label-${val.id}-${prefixID}${id}`).text(response.data[id][val.id])
                // $(`#input-${val.id}-${prefixID}${id}`).val(response.data[id][val.id])
            })
        }
        closeEditRows()
    }).catch((e)=>{
        let errors = e.responseJSON.errors
        errors.map(item =>{
            $('#error-'+item.name).text(item.message)
            $('#error-'+item.name).show()
        })
    })
}

function gotoPage(n){
    if(flexigridState == 'idle') {
        if (n) $("#flexigrid-table").makeFlexiGrid(option, n, flexigridPer_page)
    }else{
        alert('Complete your edit or cancel before do it')
    }
}

function renderPagination(data, option = {

}){
    let first = `<li><a href="#" onclick="gotoPage(1)">First</a></li>`
    let prev = `<li><a href="#" onclick="gotoPage(${parseInt(data.curent_page) - 1})">Prev</a></li>`
    let next = `<li><a href="#" onclick="gotoPage(${parseInt(data.curent_page) + 1})">Next</a></li>`
    let last = `<li><a href="#" onclick="gotoPage(${parseInt(data.total_page)})">Last</a></li>`

    if(data.curent_page == 1){
        first = `<li><a class="disabled">First</a></li>`
        prev = `<li><a class="disabled">Prev</a></li>`
    }

    if(data.curent_page == data.total_page){
        last = `<li><a class="disabled">Last</a></li>`
        next = `<li><a class="disabled">Next</a></li>`
    }

    //Render number
    let number = '';
    let limit = 3;
    if(data.total_page > 1 && data.curent_page <= data.total_page){
        let classEl = (data.curent_page == 1) ? 'class="active"': '';
        number += `<li><a href="#" ${classEl} onclick="gotoPage(1)">1</a></li>`
        let i = Math.max(2, parseInt(data.curent_page) - limit);
        if(i > 2) number += `<li><a class="disabled">...</a></li>`
        for(; i< Math.min(parseInt(data.curent_page)+limit+1, parseInt(data.total_page)); i++){
            classEl = (data.curent_page == i) ? 'class="active"': '';
            number += `<li><a href="#" ${classEl} onclick="gotoPage(${i})">${i}</a></li>`
        }
        if(i != data.total_page) number += `<li><a class="disabled">...</a></li>`
        classEl = (data.curent_page == data.total_page) ? 'class="active"': '';
        number += `<li><a href="#" ${classEl} onclick="gotoPage(${data.total_page})">${data.total_page}</a></li>`
    }

    let html = first+prev+number+next+last;
    return html
}