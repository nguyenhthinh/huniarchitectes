<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            //$table->increments('id');
            //$table->timestamps();
            $table->integer('post_id')->unsigned();
            $table->text('gallery')->nullable();
            $table->string('video')->nullable();
            $table->string('location')->nullable();
            $table->string('year')->nullable();
            $table->string('area')->nullable();
            $table->string('cost')->nullable();
            $table->string('designer')->nullable();
            $table->string('description')->nullable();
            $table->foreign('post_id', 'project_ibfk_1')->references('post_id')->on('post')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
