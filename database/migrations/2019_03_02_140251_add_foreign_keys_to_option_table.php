<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('option', function(Blueprint $table)
		{
			$table->foreign('opt_lang', 'option_ibfk_2')->references('lang_id')->on('language')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('option', function(Blueprint $table)
		{
			$table->dropForeign('option_ibfk_2');
		});
	}

}
