<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('article_id')->unsigned();
			$table->string('article_meta_title', 191);
			$table->text('article_meta_short_description', 65535);
			$table->text('content', 65535);
			$table->string('article_meta_keyword', 191);
			$table->integer('article_lang');
			$table->integer('article_category');
			$table->string('article_type', 10)->default('article');
			$table->integer('article_author');
			$table->integer('article_publish');
			$table->dateTime('article_publish_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
