<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('option', function(Blueprint $table)
		{
			$table->increments('opt_id');
			$table->string('opt_group', 50)->index('opt_group');
			$table->string('opt_name', 50)->index('opt_name');
			$table->text('opt_value')->nullable();
			$table->integer('opt_lang')->unsigned()->index('opt_lang');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('option');
	}

}
