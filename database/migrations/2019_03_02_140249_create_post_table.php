<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post', function(Blueprint $table)
		{
			$table->increments('post_id');
			$table->string('post_name', 191);
			$table->text('post_expert', 65535)->nullable();
			$table->text('post_content', 65535)->nullable();
			$table->string('post_image', 191)->nullable();
			$table->string('post_meta_title', 191)->nullable();
			$table->string('post_meta_description', 191)->nullable();
			$table->string('post_meta_keyword', 191)->nullable();
			$table->integer('post_lang')->unsigned()->nullable()->index('post_lang');
			$table->integer('post_category')->unsigned()->nullable()->index('post_category');
			$table->string('post_type', 10)->nullable()->default('post');
			$table->integer('post_author')->unsigned()->nullable()->index('post_author');
			$table->integer('post_publish')->nullable();
			$table->dateTime('post_publish_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post');
	}

}
