<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('post', function(Blueprint $table)
		{
			$table->foreign('post_lang', 'post_ibfk_1')->references('lang_id')->on('language')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('post_category', 'post_ibfk_2')->references('cat_id')->on('category')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('post_author', 'post_ibfk_3')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('post', function(Blueprint $table)
		{
			$table->dropForeign('post_ibfk_1');
			$table->dropForeign('post_ibfk_2');
			$table->dropForeign('post_ibfk_3');
		});
	}

}
