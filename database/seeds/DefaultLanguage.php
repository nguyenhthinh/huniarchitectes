<?php

use Illuminate\Database\Seeder;

class DefaultLanguage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language')->insert([
            'lang_id'=> 1,
            'lang_name' => 'Tiếng việt',
            'lang_code' => 'vi'
        ]);
        DB::table('language')->insert([
            'lang_id'=> 2,
            'lang_name' => 'English',
            'lang_code' => 'en'
        ]);
        DB::table('language')->insert([
            'lang_id'=> 3,
            'lang_name' => 'Francais',
            'lang_code' => 'fr'
        ]);
    }
}
