<?php

use Illuminate\Database\Seeder;

class DefaultCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['post', 'page', 'project', 'block'];
        $langs = [1, 2, 3];
        $i = 1;
        foreach($types as $type){
            foreach($langs as $lang){
                DB::table('category')->insert([
                    'cat_id' => $i,
                    'cat_name' => 'uncategorized',
                    'cat_lang' => $lang,
                    'cat_parent' => 0,
                    'cat_type' => $type
                ]);
                if($type != 'post'){
                    DB::table('category_'.$type)->insert([
                        'cat_id' => $i
                    ]);

                    DB::table('alias')->insert([
                        'obj_id' => $i,
                        'obj_type' => 'category_'.$type,
                        'alias' => 'uncategorized_'.$type.'_'.$lang
                    ]);
                }else{
                    DB::table('alias')->insert([
                        'obj_id' => $i,
                        'obj_type' => 'category_'.$type,
                        'alias' => 'uncategorized_'.$lang
                    ]);
                }


                $i++;
            }
        }
    }
}
