<?php

namespace App\Providers;


use App\Repositories\Category\CategoryInterface;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Option\OptionInterface;
use App\Repositories\Option\OptionRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\PostInterface;
use App\Repositories\Roles\RoleInterface;
use App\Repositories\Roles\RoleRepository;
use App\Repositories\Users\UserInterface;
use App\Repositories\Users\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(RoleInterface::class, RoleRepository::class);
        $this->app->bind(CategoryInterface::class, CategoryRepository::class);
        $this->app->bind(PostInterface::class, PostRepository::class);
        $this->app->bind(OptionInterface::class, OptionRepository::class);
    }
}
