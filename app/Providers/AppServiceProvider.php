<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Relation::morphMap([
            'category_post' => 'App\Models\Category',
            'category_project' => 'App\Models\CategoryProject',
            'category_page' => 'App\Models\CategoryPage',
            'category_block' => 'App\Models\CategoryBlock',
            'post_post' => 'App\Models\Post',
            'post_project' => 'App\Models\Project',
            'post_page' => 'App\Models\Page',
            'post_block' => 'App\Models\Block',
            //'post_product' => 'App\Models\Product',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Iber\Generator\ModelGeneratorProvider');
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
            $this->app->register('User11001\EloquentModelGenerator\EloquentModelGeneratorProvider');
        }
    }
}
