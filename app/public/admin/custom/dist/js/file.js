"use strict";

function getImage(url) {
  var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'thumb';
  //check has 'uploads/'
  var res = url.match(/uploads\//g);
  if (res) return url;else {
    var ext = ['jpg', 'png', 'gif', 'jpeg'];

    for (var i = 0; i < ext.length; i++) {
      var regex = new RegExp(ext[i], 'g'); //console.log(url.match(regex));

      if (url.match(regex)) {
        return '/uploads/images/' + size + '/' + url;
      }
    }
  }
}