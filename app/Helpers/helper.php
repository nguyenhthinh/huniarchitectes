<?php
use Illuminate\Support\Facades\DB;

function getLang($lang = null){
    $lang = ($lang)?$lang: app()->getLocale();
    $lang = DB::table('language')->where('lang_code', $lang)->first();
    return $lang;
}
function getLangID($lang = null){
    $lang = getLang($lang);
    return $lang->lang_id;
}

function getTypeName($name){
    $types = [
        'post' => 'Bài viết',
        'project' => 'Dự án',
        'page' => 'Trang',
        'block' => 'Khối nội dung'
    ];
    if(isset($types[$name])) return $types[$name];
    else return 'undefined';
}

function getOption($group, $name, $lang = null){
    $lang = ($lang) ? $lang : getLangID();
    $option = DB::table('option')
        ->where('opt_group', $group)
        ->where('opt_name', $name)
        ->where('opt_lang', $lang)
        ->first();
    if($option) return $option->opt_value;
    else return '';
}

function showThumb($url){
    return App\Helpers\Uploader::getImagePath($url);
}


function imageUrl($url, $size = 'original'){
    return App\Helpers\Uploader::getImagePath($url, $size);
}