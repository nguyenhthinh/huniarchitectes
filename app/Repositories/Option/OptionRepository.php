<?php
namespace App\Repositories\Option;

use App\Models\Option;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class OptionRepository extends BaseRepository implements OptionInterface
{

    public function model()
    {
        return Option::class;
    }

    public static function createOption($indata)
    {
        // TODO: Implement createOption() method.
    }

    public static function getOption($cond = [])
    {
        // TODO: Implement getOption() method.
    }

    public static function updateOption($indata)
    {
        // TODO: Implement updateOption() method.
    }

    public static function deleteOption($id)
    {
        // TODO: Implement deleteOption() method.
    }

    public static function updateOrCreateOption($indata)
    {
        // TODO: Implement updateOrCreateOption() method.
        $option = Option::where('opt_name', $indata['opt_name'])
            ->where('opt_group',$indata['opt_group'])
            ->where('opt_lang',$indata['opt_lang'])
            ->first();
        if($option){
            Option::where('opt_name', $indata['opt_name'])
                ->where('opt_group',$indata['opt_group'])
                ->where('opt_lang',$indata['opt_lang'])
                ->update($indata);
        }else{
            Option::create($indata);
        }
    }
}