<?php
namespace App\Repositories\Option;

interface OptionInterface
{
    public static function updateOrCreateOption($indata);
    public static function createOption($indata);
    public static function getOption($cond = []);
    public static function updateOption($indata);
    public static function deleteOption($id);
}