<?php
namespace App\Repositories\Article;

interface ArticleInterface
{
    public static function listArticle($cond = []);
    public static function getArticle($cond = []);
    public static function createArticle($data, $type='post_post');
    public static function updateArticle($data, $id, $type='post_post');
    public static function quickChange($data, $id);
    public static function deleteArticle($id, $type = 'post_post');
}