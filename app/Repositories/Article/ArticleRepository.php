<?php
namespace App\Repositories\Article;

use App\Models\Alias;
use App\Models\Category;
use App\Models\Article;
use App\Models\Project;
use App\Models\Page;
use App\Models\Block;
use App\Repositories\BaseRepository;
use App\Repositories\Article\ArticleInterface;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class ArticleRepository extends BaseRepository implements ArticleInterface
{

    public function model()
    {
        return Article::class;
    }

    public static function listArticle($cond = [])
    {
        //dd($cond);
        // TODO: Implement listArticle() method.
        $result = Article::with([
            'alias' => function($q) use ($cond){},
            'category' => function($q){},
            'article' => function($q) use ($cond){
                $q->with('alias');
            },
            'block' => function($q) use ($cond){
                $q->with('alias');
            },
            'page' => function($q) use ($cond){
                $q->with('alias');
            }
        ])->when((isset($cond['name'])), function ($query) use ($cond){
            $query->where('article_meta_title', 'like', "%{$cond['title']}%");
        })->when((isset($cond['type'])), function ($query) use ($cond){
            $query->where('article_type', $cond['type']);
        })->when((isset($cond['parent'])), function ($query) use ($cond){
            $query->where('article_category', $cond['parent']);
        })->when((isset($cond['lang'])), function ($query) use ($cond){
            $query->where('article_lang', $cond['lang']);
        })->when((isset($cond['id'])), function ($query) use ($cond){
            $query->where('article_id', $cond['id']);
        })->when((isset($cond['alias'])), function ($query) use ($cond){
            $query->when(($cond['type'] == 'news'), function ($q) use ($cond){
                $q->whereHas('news', function($q2) use ($cond){
                    $q2->whereHas('alias', function($q3) use ($cond){
                        $q3->where('alias', $cond['alias']);
                    });
                });
            });
            $query->when(($cond['type'] == 'block'), function ($q) use ($cond){
                $q->whereHas('block', function($q2) use ($cond){
                    $q2->whereHas('alias', function($q3) use ($cond){
                        $q3->where('alias', $cond['alias']);
                    });
                });
            });
            $query->when(($cond['type'] == 'page'), function ($q) use ($cond){
                $q->whereHas('page', function($q2) use ($cond){
                    $q2->whereHas('alias', function($q3) use ($cond){
                        $q3->where('alias', $cond['alias']);
                    });
                });
            });
            $query->when(($cond['type'] == 'news'), function ($q) use ($cond){
                $q->whereHas('alias', function($q2) use ($cond){
                    $q2->where('alias', $cond['alias']);
                });
            });
        });

        return $result;
    }

    public static function createArticle($data , $type='Article_Article')
    {
        // TODO: Implement createArticle() method.
        $alias = $data['Article_alias'];
        unset($data['Article_alias']);


        if($type == 'Article_project'){
            $project = [];
            if(isset($data['project'])){
                $project = $data['project'];
                unset($data['project']);
            }
            $Article = Article::create($data);
            $project['Article_id'] = $Article->Article_id;
            Project::insert($project);
        }
        elseif($type == 'Article_page'){
            $page = [];
            if(isset($data['page'])){
                $page = $data['page'];
                unset($data['page']);
            }
            $Article = Article::create($data);
            $page['Article_id'] = $Article->Article_id;
            Page::insert($page);
        }
        elseif($type == 'Article_block'){
            $block = [];
            if(isset($data['block'])){
                $block = $data['block'];
                unset($data['block']);
            }
            $Article = Article::create($data);
            $block['Article_id'] = $Article->Article_id;
            Block::insert($block);
        }
        else{
            $Article = Article::create($data);
        }


        Alias::insert([
            'obj_id' => $Article->Article_id,
            'obj_type' => $type,
            'alias' => Str::slug($alias)
        ]);
        return $Article;
    }

    public static function quickChange($data, $id){
        Article::where('Article_id', $id)->update($data);
    }

    public static function updateArticle($data, $id, $type = 'Article_Article')
    {
        // TODO: Implement updateArticle() method.
        // TODO: Implement createArticle() method.
        $alias = $data['Article_alias'];
        unset($data['Article_alias']);


        if($type == 'Article_project'){
            $project = [];
            if(isset($data['project'])){
                $project = $data['project'];
                unset($data['project']);
            }
            $Article = Article::where('Article_id', $id)->update($data);
            Project::where('Article_id', $id)->delete();
            $project['Article_id'] = $id;
            Project::insert($project);
        }
        elseif($type == 'Article_page'){
            $page = [];
            if(isset($data['page'])){
                $page = $data['page'];
                unset($data['page']);
            }
            $Article = Article::where('Article_id', $id)->update($data);
            $page['Article_id'] = $id;
            Page::insert($page);
        }
        elseif($type == 'Article_block'){
            $block = [];
            if(isset($data['block'])){
                $block = $data['block'];
                unset($data['block']);
            }
            $Article = Article::where('Article_id', $id)->update($data);
            $block['Article_id'] = $id;
            Block::insert($block);
        }
        else{
            $Article = Article::where('Article_id', $id)->update($data);
        }

        Alias::where('obj_id', $id)->where('obj_type', $type)->delete();
        Alias::insert([
            'obj_id' => $id,
            'obj_type' => $type,
            'alias' => Str::slug($alias)
        ]);
        return $Article;
    }

    public static function deleteArticle($id, $type = 'Article_Article')
    {
        // TODO: Implement deleteCategory() method.
        Article::find($id)->delete();
        Alias::where('obj_id', $id)->where('obj_type', $type)->delete();
    }

    public static function getArticle($cond = [])
    {
        // TODO: Implement getArticle() method.
        return self::listArticle($cond);
    }
}