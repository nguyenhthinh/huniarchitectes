<?php
namespace App\Repositories\Users;

interface UserInterface
{
    public static function listUserInformation($cond = []);
    public static function getUserInformation($id);

    public static function createUser($data);
}