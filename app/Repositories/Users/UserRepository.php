<?php
namespace App\Repositories\Users;

use App\Models\User;
use App\Models\UsersProfile;
use App\Models\UsersSkill;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use DB;

class UserRepository extends BaseRepository implements UserInterface{

    public function model()
    {
        return User::class;
    }

    public static function getUserInfo($cond = [])
    {
        $info = User::whereHas('userProfiles', function ($query) use ($cond){
                                $query->when(isset($cond['query']), function ($query2) use ($cond){
                                    return $query2->where('firstname','like',$cond['query'])
                                                    ->orWhere('lastname','like',$cond['query']);
                                });
                            })
                        ->whereHas('userSkills', function($query) use ($cond){
                            $query->when(isset($cond['skill_id']), function ($query2) use ($cond){
                                return $query2->whereIn('skill_id',$cond['skill_id']);
                            });
                        })
                        ->with([
                            'userProfiles',
                            'userSkills'=>function($query){
                                $query->with('skill');
                            },
                            'usersDevice',
                            'empLevels'
                    ]);
        $info->when(isset($cond['id']), function ($query) use ($cond){
            return $query->where('id',$cond['id']);
        });
        //dd($info->toSql());
        return $info;
    }

    public static function getUserInformation($id)
    {
        // TODO: Implement getUserInformation() method.
        $info = self::getUserInfo(['id'=>$id]);
        return $info->first();
    }
    public static function listUserInformation($cond = [])
    {
        // TODO: Implement listUserInformation() method.

        $info = self::getUserInfo($cond);
        if(isset($cond['pagination']))
            return $info->paginate($cond['pagination'])->appends(request()->query());
        else
            return $info->paginate()->appends(request()->query());
    }

    public static function createUser($data)
    {
        // TODO: Implement createUser() method.
        DB::beginTransaction();
        try {
            $user = new User();
            $user->name = $data->username;
            $user->password = bcrypt('infra911');
            $user->email = $data->email;
            $user->save();

            $user->userProfiles()->create(
                [
                    'firstname' => $data->firstname,
                    'lastname' => $data->lastname,
                    'level_id' => $data->level,
                    'join_data' => Carbon::now(),
                ]
            );
            foreach($data->role as $role){
                $user->roles()->attach($role);
            }

            foreach($data->skill as $skill){
                $user->userSkills()->create([
                    'skill_id' => $skill
                ]);
            }

            DB::commit();
            // all good
            return 'done';
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return $e;
        }
    }
}