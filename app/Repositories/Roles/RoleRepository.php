<?php
namespace App\Repositories\Roles;

use App\Models\Role;
use App\Repositories\BaseRepository;
use DB;

class RoleRepository extends BaseRepository implements RoleInterface{

    public function model()
    {
        return Role::class;
    }

    public static function getRole($cond = [])
    {
        $info = Role::when(isset($cond['id']), function ($query) use ($cond){
            return $query->where('id',$cond['id']);
        })->when(isset($cond['query']), function ($query) use ($cond){
            return $query->where('name','like',"%{$cond['query']}%");
        });
        return $info;
    }

    public static function getRoleInfo($id)
    {
        //TODO: Implement getRoleInfo() method.
        $info = self::getRole(['id'=>$id]);
        return $info->first();
    }

    public static function createRole($data)
    {
        // TODO: Implement createUser() method.
        DB::beginTransaction();

        try {


            DB::commit();
            // all good
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return false;
        }
    }

    public static function listRoleInfo($cond = [])
    {
        // TODO: Implement listRoleInfo() method.
        $info = self::getRole($cond);
        return $info->get();
    }
}