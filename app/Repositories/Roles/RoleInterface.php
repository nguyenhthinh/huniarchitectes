<?php
namespace App\Repositories\Roles;

interface RoleInterface
{
    public static function listRoleInfo($cond = []);
    public static function getRoleInfo($id);

    public static function createRole($data);
}