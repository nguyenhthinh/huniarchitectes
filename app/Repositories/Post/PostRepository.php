<?php
namespace App\Repositories\Post;

use App\Models\Alias;
use App\Models\Category;
use App\Models\Post;
use App\Models\Project;
use App\Models\Page;
use App\Models\Block;
use App\Repositories\BaseRepository;
use App\Repositories\Post\PostInterface;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class PostRepository extends BaseRepository implements PostInterface
{

    public function model()
    {
        return Post::class;
    }

    public static function listPost($cond = [])
    {
        //dd($cond);
        // TODO: Implement listPost() method.
        $result = Post::with([
            'alias' => function($q) use ($cond){},
            'category' => function($q){},
            'project' => function($q) use ($cond){
                $q->with('alias');
            },
            'block' => function($q) use ($cond){
                $q->with('alias');
            },
            'page' => function($q) use ($cond){
                $q->with('alias');
            }
        ])->when((isset($cond['name'])), function ($query) use ($cond){
            $query->where('post_name', 'like', "%{$cond['name']}%");
        })->when((isset($cond['type'])), function ($query) use ($cond){
            $query->where('post_type', $cond['type']);
        })->when((isset($cond['parent'])), function ($query) use ($cond){
            $query->where('post_category', $cond['parent']);
        })->when((isset($cond['lang'])), function ($query) use ($cond){
            $query->where('post_lang', $cond['lang']);
        })->when((isset($cond['id'])), function ($query) use ($cond){
            $query->where('post_id', $cond['id']);
        })->when((isset($cond['alias'])), function ($query) use ($cond){
            $query->when(($cond['type'] == 'project'), function ($q) use ($cond){
                $q->whereHas('project', function($q2) use ($cond){
                    $q2->whereHas('alias', function($q3) use ($cond){
                        $q3->where('alias', $cond['alias']);
                    });
                });
            });
            $query->when(($cond['type'] == 'block'), function ($q) use ($cond){
                $q->whereHas('block', function($q2) use ($cond){
                    $q2->whereHas('alias', function($q3) use ($cond){
                        $q3->where('alias', $cond['alias']);
                    });
                });
            });
            $query->when(($cond['type'] == 'page'), function ($q) use ($cond){
                $q->whereHas('page', function($q2) use ($cond){
                    $q2->whereHas('alias', function($q3) use ($cond){
                        $q3->where('alias', $cond['alias']);
                    });
                });
            });
            $query->when(($cond['type'] == 'post'), function ($q) use ($cond){
                $q->whereHas('alias', function($q2) use ($cond){
                    $q2->where('alias', $cond['alias']);
                });
            });
        });

        return $result;
    }

    public static function createPost($data , $type='post_post')
    {
        // TODO: Implement createPost() method.
        $alias = $data['post_alias'];
        unset($data['post_alias']);


        if($type == 'post_project'){
            $project = [];
            if(isset($data['project'])){
                $project = $data['project'];
                unset($data['project']);
            }
            $post = Post::create($data);
            $project['post_id'] = $post->post_id;
            Project::insert($project);
        }
        elseif($type == 'post_page'){
            $page = [];
            if(isset($data['page'])){
                $page = $data['page'];
                unset($data['page']);
            }
            $post = Post::create($data);
            $page['post_id'] = $post->post_id;
            Page::insert($page);
        }
        elseif($type == 'post_block'){
            $block = [];
            if(isset($data['block'])){
                $block = $data['block'];
                unset($data['block']);
            }
            $post = Post::create($data);
            $block['post_id'] = $post->post_id;
            Block::insert($block);
        }
        else{
            $post = Post::create($data);
        }


        Alias::insert([
            'obj_id' => $post->post_id,
            'obj_type' => $type,
            'alias' => Str::slug($alias)
        ]);
        return $post;
    }

    public static function quickChange($data, $id){
        Post::where('post_id', $id)->update($data);
    }

    public static function updatePost($data, $id, $type = 'post_post')
    {
        // TODO: Implement updatePost() method.
        // TODO: Implement createPost() method.
        $alias = $data['post_alias'];
        unset($data['post_alias']);


        if($type == 'post_project'){
            $project = [];
            if(isset($data['project'])){
                $project = $data['project'];
                unset($data['project']);
            }
            $post = Post::where('post_id', $id)->update($data);
            Project::where('post_id', $id)->delete();
            $project['post_id'] = $id;
            Project::insert($project);
        }
        elseif($type == 'post_page'){
            $page = [];
            if(isset($data['page'])){
                $page = $data['page'];
                unset($data['page']);
            }
            $post = Post::where('post_id', $id)->update($data);
            $page['post_id'] = $id;
            Page::insert($page);
        }
        elseif($type == 'post_block'){
            $block = [];
            if(isset($data['block'])){
                $block = $data['block'];
                unset($data['block']);
            }
            $post = Post::where('post_id', $id)->update($data);
            $block['post_id'] = $id;
            Block::insert($block);
        }
        else{
            $post = Post::where('post_id', $id)->update($data);
        }

        Alias::where('obj_id', $id)->where('obj_type', $type)->delete();
        Alias::insert([
            'obj_id' => $id,
            'obj_type' => $type,
            'alias' => Str::slug($alias)
        ]);
        return $post;
    }

    public static function deletePost($id, $type = 'post_post')
    {
        // TODO: Implement deleteCategory() method.
        Post::find($id)->delete();
        Alias::where('obj_id', $id)->where('obj_type', $type)->delete();
    }

    public static function getPost($cond = [])
    {
        // TODO: Implement getPost() method.
        return self::listPost($cond);
    }
}