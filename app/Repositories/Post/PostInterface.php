<?php
namespace App\Repositories\Post;

interface PostInterface
{
    public static function listPost($cond = []);
    public static function getPost($cond = []);
    public static function createPost($data, $type='post_post');
    public static function updatePost($data, $id, $type='post_post');
    public static function quickChange($data, $id);
    public static function deletePost($id, $type = 'post_post');
}