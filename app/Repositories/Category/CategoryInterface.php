<?php
namespace App\Repositories\Category;

interface CategoryInterface
{
    public static function getCategory($cond);
    public static function addCategory($indata, $type = 'category_post');
    public static function updateCategory($indata, $id, $type = 'category_post');
    public static function deleteCategory($id, $type = 'category_post');
    public static function reOrderCategory($id, $order, $type = 'category_post');
}