<?php
namespace App\Repositories\Category;

use App\Models\Alias;
use App\Models\Category;
use App\Models\CategoryProject;
use App\Models\CategoryBlock;
use App\Models\CategoryPage;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class CategoryRepository extends BaseRepository implements CategoryInterface
{

    public function model()
    {
        return Category::class;
    }

    public static function getCategory($cond)
    {
        // TODO: Implement getCategory() method.
        $result = Category::with([
                'alias' => function($q) use ($cond){},
                'parent' => function($q){},
                'project' => function($q) use ($cond){
                    $q->with('alias');
                },
                'block' => function($q) use ($cond){
                    $q->with('alias');
                },
                'page' => function($q) use ($cond){
                    $q->with('alias');
                }
            ])
            ->when((isset($cond['name'])), function ($query) use ($cond){
                $query->where('cat_name', 'like', $cond['name']);
            })->when((isset($cond['type'])), function ($query) use ($cond){
                $query->where('cat_type', $cond['type']);
            })->when((isset($cond['parent'])), function ($query) use ($cond){
                $query->where('cat_parent', $cond['parent']);
            })->when((isset($cond['lang'])), function ($query) use ($cond){
                $query->where('cat_lang', $cond['lang']);
            })->when((isset($cond['alias'])), function ($query) use ($cond){
                $query->when(($cond['type'] == 'project'), function ($q) use ($cond){
                    $q->whereHas('project', function($q2) use ($cond){
                       $q2->whereHas('alias', function($q3) use ($cond){
                           $q3->where('alias', $cond['alias']);
                       });
                    });
                });
                $query->when(($cond['type'] == 'block'), function ($q) use ($cond){
                    $q->whereHas('block', function($q2) use ($cond){
                        $q2->whereHas('alias', function($q3) use ($cond){
                            $q3->where('alias', $cond['alias']);
                        });
                    });
                });
                $query->when(($cond['type'] == 'page'), function ($q) use ($cond){
                    $q->whereHas('page', function($q2) use ($cond){
                        $q2->whereHas('alias', function($q3) use ($cond){
                            $q3->where('alias', $cond['alias']);
                        });
                    });
                });
                $query->when(($cond['type'] == 'post'), function ($q) use ($cond){
                    $q->whereHas('alias', function($q2) use ($cond){
                        $q2->where('alias', $cond['alias']);
                    });
                });
            });

        return $result;
    }

    public static function addCategory($indata, $type = 'category_post')
    {
        // TODO: Implement addCategory() method.
        $cat_alias = $indata['cat_alias'];
        unset( $indata['cat_alias']);
        $cat =  Category::create($indata);
        $types = explode('_', $type);

        if($types[1] == 'project'){
            CategoryProject::insert(['cat_id'=>$cat->cat_id]);
        }
        if($types[1] == 'block'){
            CategoryBlock::insert(['cat_id'=>$cat->cat_id]);
        }
        if($types[1] == 'page'){
            CategoryPage::insert(['cat_id'=>$cat->cat_id]);
        }

        Alias::insert([
            'obj_id'=>$cat->cat_id,
            'alias'=>Str::slug($cat_alias),
            'obj_type'=>$type
        ]);
        return ($cat);
    }

    public static function updateCategory($indata, $id, $type = 'category_post')
    {
        // TODO: Implement updateCategory() method.
        $cat_alias = $indata['cat_alias'];
        unset( $indata['cat_alias']);
        Category::find($id)->update($indata);
        Alias::where('obj_id', $id)->where('obj_type', $type)->update(['alias'=>$cat_alias]);
    }

    public static function deleteCategory($id, $type = 'category_post')
    {
        // TODO: Implement deleteCategory() method.
        Category::find($id)->delete();
        Alias::where('obj_id', $id)->where('obj_type', $type)->delete();
    }

    public static function reOrderCategory($id, $order, $type = 'category_post')
    {
        // TODO: Implement reOrderCategory() method.
        Category::find($id)->update(['cat_order' => $order]);
    }
}