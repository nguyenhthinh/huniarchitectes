<?php

namespace App\Http\Controllers\Administrator;

use App\Models\User;
use App\Repositories\Option\OptionInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    private $optRepo;
    //
    public function __construct(
        OptionInterface $optInterface
    )
    {
        $this->optRepo = $optInterface;
    }

    public function index(){
        return view('administrator.main');
    }
    public function setting(){
        return view('administrator.setting');
    }

    public function settingUpdate(Request $request){
        foreach($request->option as $opt_key => $optgroup){
            foreach($optgroup as $opt => $val){
                $indata = [
                    'opt_group' => $opt_key,
                    'opt_name' => $opt,
                    'opt_value' => $val,
                    'opt_lang' => getLangID()
                ];
                $this->optRepo::updateOrCreateOption($indata);
            }
        }
        return response()->json($request->option);
    }

    public function changepass(){
        return view('administrator.changepass');
    }
    public function changepassProcess(Request $request){
//        dd($request->all(), auth()->user());
        $checkpass = User::find(auth()->user()->id);
        //dd($checkpass, Hash::make($request->oldpass), Hash::check($request->oldpass, $checkpass->password));
        if(!Hash::check($request->oldpass, $checkpass->password)) return response('Mật khẫu cũ không đúng', 400);
        else{
            if(strlen($request->newpass) < 6) return response('Mật khẫu mới phải trên 6 ký tự', 400);
            else{
                if($request->newpass != $request->cnewpass) return response('Mật khẫu mới không khớp', 400);
                else{
                    $checkpass->password = Hash::make($request->newpass);
                    $checkpass->save();
                    return response('Đổi mật khẩu thành công');
                }
            }
        }
    }
}
