<?php

namespace App\Http\Controllers\Administrator;

use App\Repositories\Category\CategoryInterface;
use App\Repositories\Post\PostInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PostController extends Controller
{
    //
    private $catRepo;
    private $postRepo;

    function __construct(
        CategoryInterface $catInterface,
        PostInterface $postInterface
    )
    {
        $this->catRepo = $catInterface;
        $this->postRepo = $postInterface;
    }

    public function index(Request $request, $type='post'){
        $cond = [
            'lang'=>getLangID(),
            'type'=>$type
        ];
        $cond2 = $cond;

        if($request->name) $cond['name'] = $request->name;
        if($request->parent) $cond['parent'] = $request->parent;
        $orderBy = ($request->orderBy)? explode('-', $request->orderBy) : false;
        $posts = ($orderBy) ? 
                            $this->postRepo::listPost($cond)->orderBy($orderBy[0],$orderBy[1])->paginate(15) :  
                            $this->postRepo::listPost($cond)->orderBy('post_order', 'asc')->orderBy('updated_at','desc')->orderBy('post_publish_date','desc')->paginate(15);
        $cates = $this->catRepo::getCategory($cond2)->get();
        return view('administrator.post.list', compact('posts', 'cates', 'type', 'request'));
    }

    public function add(Request $request, $type='post'){
        //dump($type);
        $cond = [
            'lang'=>getLangID(),
            'type'=>$type
        ];
        $cond2 = $cond;

        if($request->name) $cond['name'] = $request->name;
        if($request->parent) $cond['parent'] = $request->parent;

        $cates = $this->catRepo::getCategory($cond2)->get();
        //dd($categories);
        return view('administrator.post.add', compact( 'cates', 'type', 'request'));
    }

    public function addProcess(Request $request, $type = 'post')
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'parent' => 'numeric|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $indata = array(
                'post_name' => $request->name,
                'post_alias' => $request->alias ? $request->alias : $request->name,
                'post_category'=> $request->parent,
                'post_expert'=> $request->description,
                'post_lang' => getLangID(),
                'post_type' => $type,
                'post_image' => $request->image,
                'post_content' => $request->post('content'),
                'post_meta_title' => $request->meta_title,
                'post_meta_description' => $request->meta_description,
                'post_author'=> auth()->user()->id,
                'post_publish' => 1,
                'post_publish_date'=>Carbon::now()
            );
            if($type == 'project'){
                $indata['project'] = [
                    'gallery' => implode(',', $request->project_gallery),
                    'video'=>$request->project_video,
                    'location' => $request->project_location,
                    'year' => $request->project_year,
                    'area' => $request->project_area,
                    'cost' => $request->project_cost,
                    'designer' => $request->project_designer,
                    'description' => $request->project_description
                ];

            }
            $this->postRepo::createPost($indata, 'post_'.$type);
//            return response()->json(['message'=>'Đã thêm vào CSDL'],400);
            return response()->json(['message'=>'Đã thêm vào CSDL']);
        }
    }

    public function edit(Request $request, $type='post', $id){
        //dump($type);
        $cond = [
            'lang'=>getLangID(),
            'type'=>$type
        ];
        $cond2 = $cond;

        if($request->name) $cond['name'] = $request->name;
        if($request->parent) $cond['parent'] = $request->parent;

        $cates = $this->catRepo::getCategory($cond2)->get();

        $cond3 = $cond2;
        $cond3['id'] = $id;
        $post = $this->postRepo::getPost($cond3)->first();
        if($post){
            //dd($categories);
            //dd($post);
            return view('administrator.post.edit', compact( 'cates', 'type', 'request', 'post'));
        }
        else abort(404);
    }

    public function editProcess(Request $request, $type = 'post')
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'parent' => 'numeric|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $indata = array(
                'post_name' => $request->name,
                'post_alias' => $request->alias ? $request->alias : $request->name,
                'post_category'=> $request->parent,
                'post_expert'=> $request->description,
                'post_lang' => getLangID(),
                'post_type' => $type,
                'post_image' => $request->image,
                'post_content' => $request->post('content'),
                'post_meta_title' => $request->meta_title,
                'post_meta_description' => $request->meta_description,
                //'post_author'=> auth()->user()->id,
                //'post_publish' => 1,
                //'post_publish_date'=>Carbon::now()
            );
            if($type == 'project'){
                $indata['project'] = [
                    'gallery' => implode(',', $request->project_gallery),
                    'video'=>$request->project_video,
                    'location' => $request->project_location,
                    'year' => $request->project_year,
                    'area' => $request->project_area,
                    'cost' => $request->project_cost,
                    'designer' => $request->project_designer,
                    'description' => $request->project_description
                ];

            }
            $this->postRepo::updatePost($indata, $request->post_id, 'post_'.$type);
//            return response()->json(['message'=>'Đã thêm vào CSDL'],400);
            return response()->json(['message'=>'Đã thêm vào CSDL']);
        }
    }

    public function quickChange(Request $request, $type = 'post')
    {
        
        if($request->action == 'order') $indata = ['post_order'=>$request->value];
        if($request->action == 'status') $indata = ['post_publish'=>$request->value];
        $this->postRepo::quickChange($indata, $request->id);
//            return response()->json(['message'=>'Đã thêm vào CSDL'],400);
        return response()->json(['message'=>'Cập nhật thành công']);
        
    }


    public function delete(Request $request, $type='post'){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $this->postRepo::deletePost($request->id, 'post_'.$type);
            return response()->json(['message'=>'Đã xóa khỏi CSDL']);
        }
    }
}
