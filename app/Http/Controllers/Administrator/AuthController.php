<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public static function login(){
        return view('administrator.auth.login');
    }

    public static function loginProcess(Request $request){
        $credential = $request->only('email', 'password');
        if(auth()->attempt($credential)){
            return response()->json(['status'=>true, 'message'=> 'Đăng nhập thành công, hệ thống sẽ tự động chuyển đến trang quản trị']);
        }
        else{
            return response()->json(['status'=>false, 'message'=> 'Đăng nhập thất bại, tài khoản hoặc mật khẩu không đúng.'], 401);
        }
    }

    public function logout(){
        auth()->logout();
        return redirect(route('admin-login'));
    }
}
