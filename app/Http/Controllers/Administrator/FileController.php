<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Uploader;

class FileController extends Controller
{
    public function upload(Request $request){
        //dd($request->file('upload'));
        $uploaded = Uploader::upload($request->file('upload'), 'image');
        $result = [
            "uploaded" => 1,
            "fileName" => $uploaded['fullpath'],
            "url" => Uploader::getImagePath($uploaded['fullpath'], 'origin'),
//            "error" => [
//                "message" => "A file with the same name already exists."
//            ]
        ];
        return response()->json($result);
    }

    public function uploadInput(Request $request){

        $uploaded = Uploader::upload($request->file('file'), 'image');
        //dd($uploaded);
        $result = [
            'file' => $uploaded['filename'],
            'thumb' => Uploader::getImagePath($uploaded['filename'])
        ];
        return response()->json($result);
    }

    public function browser(Request $request){
        return view('administrator.file');
    }
}
