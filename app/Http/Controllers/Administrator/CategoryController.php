<?php

namespace App\Http\Controllers\Administrator;

use App\Repositories\Category\CategoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CategoryController extends Controller
{
    //
    private $catRepo;

    function __construct(
        CategoryInterface $catInterface
    )
    {
        $this->catRepo = $catInterface;
    }

    public function index(Request $request, $type='post'){
        $cond = [
            'lang'=>getLangID(),
            'type'=>$type
        ];
        $cond2 = $cond;

        if($request->name) $cond['name'] = $request->name;
        if($request->parent) $cond['parent'] = $request->parent;

        $categories = $this->catRepo::getCategory($cond)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->paginate(15);
        $cates = $this->catRepo::getCategory($cond2)->get();
//        dd($categories[0]->$type);
        return view('administrator.category.list', compact('categories', 'cates', 'type', 'request'));
    }

    public function add(Request $request, $type='post'){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $indata = [
                'cat_name' => $request->name,
                'cat_alias' => $request->alias ? $request->alias : $request->name,
                'cat_parent'=> $request->parent,
                'cat_image'=> $request->image,
                'cat_lang' => getLangID(),
                'cat_type' => $request->segment(3)
            ];
            $this->catRepo::addCategory($indata, 'category_'.$type);
            return response()->json(['message'=>'Đã thêm vào CSDL']);
        }
    }

    public function edit(Request $request, $type='post'){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $indata = [
                'cat_name' => $request->name,
                'cat_alias' => $request->alias ? $request->alias : $request->name,
                'cat_parent'=> $request->parent,
                'cat_image'=> $request->image,
                'cat_lang' => getLangID()
            ];
            $this->catRepo::updateCategory($indata, $request->id, 'category_'.$type);
            return response()->json(['message'=>'Đã cập nhật vào CSDL']);
        }
    }

    public function delete(Request $request, $type='post'){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $this->catRepo::deleteCategory($request->id, 'category_'.$type);
            return response()->json(['message'=>'Đã xóa khỏi CSDL']);
        }
    }
    public function reOrder(Request $request, $type='post'){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors(), 'message'=>'Dữ liêu không hợp lệ'], 400);
        }else{
            $this->catRepo::reOrderCategory($request->id, $request->order, 'category_'.$type);
            return response()->json(['message'=>'Đã xóa khỏi CSDL']);
        }
    }
}
