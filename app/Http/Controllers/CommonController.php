<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryInterface;
use App\Repositories\Post\PostInterface;
use Illuminate\Http\Request;
use Mail;


class CommonController extends Controller
{
    private $catRepo;
    private $postRepo;
    private $proCats;
    private $newsCats;


    public function __construct(
        CategoryInterface $catInterface,
        PostInterface $postInterface
    )
    {
        $this->catRepo = $catInterface;
        $this->postRepo = $postInterface;
    }

    public function index(){
	    $lang = getLangID();
	    if ($lang == '2') {
		    return redirect('en/project');
	    }
	    
        return redirect('fr/project');
    }

    public function project(Request $request, $lang){
	    $cond = [
            'lang'=>getLangID(),
            'type'=>'project'
        ];
	    $condNews = [
	    	'lang'=>getLangID(),
			'type'=>'news'
		];

	    $newsCats = $this->catRepo::getCategory($condNews)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
	    $this->proCats = $this->catRepo::getCategory($cond)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
        $proCats = $this->proCats;
        return view('frontend.project', compact('lang', 'proCats', 'newsCats'));
    }

    public function projectCategory(Request $request, $lang, $cat){
        $cond = [
            'lang'=>getLangID(),
            'type'=>'project'
        ];
		$condNews = [
			'lang'=>getLangID(),
			'type'=>'news'
		];

		$newsCats = $this->catRepo::getCategory($condNews)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
        $this->proCats = $this->catRepo::getCategory($cond)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
        $proCats = $this->proCats;
        $cond = [
            'lang'=>getLangID(),
            'type'=>'project',
            'alias'=> $cat
        ];
        $category = $this->catRepo::getCategory($cond)->first();
        if($category){
            //dd($category->toSql(), $category->getBindings());
            unset($cond['alias']);
            $cond['parent'] = $category->cat_id;
            $projects = $this->postRepo::listPost($cond)->where('post_publish', 1)->orderBy('post_order', 'asc')->orderBy('updated_at','desc')->orderBy('post_publish_date','desc')->paginate(9);
            //dd($projects->toSql(), $projects->getBindings());
            return view(
            	'frontend.project_cat',
				compact('lang', 'proCats', 'projects', 'category', 'newsCats'));
        }else abort(404);
//
    }

	public function newsCategory(Request $request, $lang, $cat){
		$cond = [
			'lang'=>getLangID(),
			'type'=>'project'
		];
		$condNews = [
			'lang'=>getLangID(),
			'type'=>'news'
		];

		$newsCats = $this->catRepo::getCategory($condNews)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
		$this->proCats = $this->catRepo::getCategory($cond)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
		$proCats = $this->proCats;
		$cond = [
			'lang'=>getLangID(),
			'type'=>'news',
			'alias'=> $cat
		];
		$category = $this->catRepo::getCategory($cond)->first();
		if($category){
			unset($cond['alias']);
			$cond['parent'] = $category->cat_id;
			$newss = $this->postRepo::listPost($cond)->where('post_publish', 1)->orderBy('post_order', 'asc')->orderBy('updated_at','desc')->orderBy('post_publish_date','desc')->paginate(9);
			return view('frontend.project_cat', compact('lang', 'proCats', 'news', 'category', 'newsCats'));
		}else abort(404);
	}

    public function projectDetail(Request $request, $lang, $cat, $post){
        $cond = [
            'lang'=>getLangID(),
            'type'=>'project'
        ];

        $this->proCats = $this->catRepo::getCategory($cond)->orderBy('cat_id', 'desc')->get();
        $proCats = $this->proCats;
        $cond = [
            'lang'=>getLangID(),
            'type'=>'project',
            'alias'=> $cat
        ];
        $category = $this->catRepo::getCategory($cond)->first();
        if($category){
            //dd($category->toSql(), $category->getBindings());
            $cond['alias'] = $post;
            $detail = true;
            $project = $this->postRepo::listPost($cond)->orderBy('updated_at','desc')->orderBy('post_publish_date','desc')->first();
            //dd($projects->toSql(), $projects->getBindings());
            if($project)
                return view('frontend.project_detail', compact('lang', 'proCats', 'project', 'category', 'detail'));
            else abort(404);
        }else abort(404);
    }

    public function contact(Request $request, $lang){
	

        $cond = [
            'lang'=>getLangID(),
            'type'=>'page',
            'alias' => 'contact'
        ];
        $vieCond = [
	        'lang'=>getLangID(),
	        'type'=>'page',
	        'alias' => 'contact_vn'
        ];
	    $post = $this->postRepo::listPost($cond)->first();
	    $viePost = $this->postRepo::listPost($vieCond)->first();
        return view('frontend.contact', compact('lang', 'post', 'viePost'));
    }

    public function introduction(Request $request, $lang){
        $cond = [
            'lang'=>getLangID(),
            'type'=>'project'
        ];

        $proCats = $this->catRepo::getCategory($cond)->orderBy('cat_order', 'asc')->orderBy('cat_id', 'desc')->get();
        $detail = true;
        $cond = [
            'lang'=>getLangID(),
            'type'=>'page',
            'alias' => 'introduction'
        ];
        $post = $this->postRepo::listPost($cond)->first();
        //dd($post->toSql(), $post->getBindings());
        return view('frontend.introduction', compact('lang', 'proCats', 'post', 'detail'));
    }

    public function sendMail(Request $request){
        //dd($request);
        Mail::send('email.contact', [
                                        'name' => $request->txtName,
                                        'email'=>$request->txtEmail,
                                        'subject'=>$request->txtSubject,
                                        'content'=>$request->txtContent
                                    ], function ($m) use ($request){
                                                        $m->from('noreply@huniarchitectes.com', 'No-Reply Huni');
                                                        $m->to(getOption('author','email'))->subject('Liên hệ mới từ huni');
        });
    }
}
