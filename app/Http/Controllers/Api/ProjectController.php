<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Uploader;
use App\Repositories\Projects\ProjectInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class ProjectController extends Controller
{
    protected $projectRepo;

    public function __construct(ProjectInterface $projectRepository)
    {
        $this->projectRepo = $projectRepository;
        $this->middleware('requiretoken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();
        $cond = [];
        $cond['user'] = $user->id;
        //$cond['pagination'] = (empty($request->pagination))?$request->pagination : 9999;
        $result = $this->projectRepo->listProject($cond);
        return response()->json(['data'=>$result, 'status'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $input = $request;
        $file = $request->file();
        //dd('STORE', $input, $file);
        Validator::make($request->all(), [
            'name' => 'required',
            'start' => 'required|date',
            'target' => 'required|date',
            'leader' => 'required',
            'member' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ])->validate();
        $image = Uploader::upload($request->file('image'));
        $indata['project'] = [
            "name"  => $request->name,
            "image"  => $image['filename'],
            "git_url"  => $request->git_url,
            "start_date"  => $request->start,
            "target_date"  => $request->target,
            "creator"  => $user->id,
            "color" => rand(1,7),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ];
        $indata["member"] = [
            [
                "user_id" => $request->leader,
                "role"=> 4,
                "join_date" => Carbon::now()
            ]
        ];
        foreach($request->member as $mem){
            $indata["member"][] = [
                "user_id" => $mem,
                "role"=> 12,
                "join_date" => Carbon::now()
            ];
        }
        $result = $this->projectRepo->createProject($indata);
        dd($result);
        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
