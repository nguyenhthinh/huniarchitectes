<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Users\UserInterface;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class UserController extends Controller
{
    protected $userRepo;

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $user = JWTAuth::toUser($request->token);
        $filter = [];
        /* $filter['id'] = int: search by user id
         * $filter['query'] - string: search by name
         * $filter['skill_id'] - int: search by skill id
         * */
        if($request->skill){
            $filter['skill_id'] = [$request->skill];
            if(is_array($request->skill)) $filter['skill_id'] = $request->skill;
        }
        if($request->pagination)
            $filter['pagination'] = $request->pagination;

        $users = $this->userRepo->listUserInformation($filter);
        return response()->json(['data'=>$users, 'status'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //dd("CREATE");
        //dd($request->all());
        Validator::make($request->all(), [
            'username' => 'required|unique:users,name|max:100',
            'email' => 'required|unique:users,email|email|max:191',
            'firstname' => 'required',
            'lastname' => 'required',
            'skill' => 'required',
            'level' => 'required',
            'role' => 'required',
        ])->validate();
        $result = $this->userRepo->createUser($request);
        return response()->json(['status'=>true, 'message'=> $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd("STORE");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = $this->userRepo->listUserInformation($id);
        return response()->json($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd("EDIT");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd("UPDATE");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd("DETROY");
    }
}
