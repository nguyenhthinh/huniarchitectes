<?php

namespace App\Http\Controllers\Api;

use App\Repositories\EmpLevels\EmpLevelInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmpLevelController extends Controller
{
    protected $emplvRepo;

    public function __construct(EmpLevelInterface $userRepository)
    {
        $this->emplvRepo = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        /* $filter['id'] = int: search by user id
         * $filter['query'] - string: search by name
         * */
        $filter['query'] = ($request->get("query")) ? $request->get("query") : '__';
        $emplvs = $this->emplvRepo->listEmpLevelInfo($filter);
        return response()->json(['data'=>$emplvs, 'status'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
