<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryInterface;
use App\Repositories\Post\PostInterface;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ConvertController extends Controller
{
    private $catRepo;
    private $postRepo;

    public function __construct(
        CategoryInterface $catInterface,
        PostInterface $postInterface
    )
    {
        $this->catRepo = $catInterface;
        $this->postRepo = $postInterface;
    }

    public function parent(Request $request){
        $parents = DB::connection('mysql2')
                        ->table('tgp_gallery_menu')
                        ->where('parent', 0)
                        ->where('cat', 'slide')
                        ->where('_lang', 'fr')
                        ->orderBy('id', 'desc')
                        ->get();
        //dd($parents);
        $i = 1;
        foreach($parents as $p){
            if($i==1){
                $indata = [
                    'cat_name' => $p->ten,
                    'cat_alias' => $p->ten,
                    'cat_parent'=> 0,
                    'cat_image'=> $p->hinh,
                    'cat_lang' => 3,
                    'cat_type' => 'project'
                ];
                $x = $this->catRepo::addCategory($indata, 'category_project');
                //dd($x);
                //echo $p->id.':&nbsp;&nbsp;'.$x->cat_id.'<hr/>';
                echo "<a target='_blank' href='http://127.0.0.1:8000/convert/child?parent={$p->id}&cat={$x->cat_id}'>".$p->id.':&nbsp;&nbsp;'.$x->cat_id."</a><hr/>";
            }
            //$i++;
        }

    }

    public function child(Request $request){
        $parents = DB::connection('mysql2')
            ->table('tgp_gallery_menu')
            ->where('parent', $request->parent)
            ->where('cat', 'slide')
            ->where('_lang', 'fr')
            ->orderBy('id', 'desc')
            ->get();
        //dd($parents);
        foreach($parents as $p){
            $indata = array(
                'post_name' => $p->ten,
                'post_alias' => $p->ten,
                'post_category'=> $request->cat,
                'post_lang' => 3,
                'post_type' => 'project',
                'post_image' => $p->hinh,
                'post_meta_title' => $p->ten,
                'post_meta_description' => $p->ten,
                'post_author'=> auth()->user()->id,
                'post_publish' => 1,
                'post_publish_date'=>Carbon::now()
            );

            $gallery = [];
            $gs = DB::connection('mysql2')->table('tgp_gallery')->where('cat', $p->id)->where('_lang', 'fr')->get();
            foreach($gs as $g){
                $gallery[] = $g->hinh;
            }

            $indata['project'] = [
                'gallery' => implode(',', $gallery),
                'video'=>$p->video,
                'location' => $p->vi_tri,
                'year' => $p->thoi_gian,
                'area' => $p->be_mat,
                'cost' => $p->chi_phi,
                'designer' => $p->thiet_ke,
                'description' => $request->chu_thich
            ];


            $x = $this->postRepo::createPost($indata, 'post_project');

            //dd($x);
            echo "<a target='_blank' href='http://127.0.0.1:8000/convert/gallery?parent={$p->id}&cat={$x->post_id}'>".$p->id.':&nbsp;&nbsp;'.$x->post_id."</a><hr/>";
        }
    }
}
