<?php

namespace App\Http\Middleware;

use Closure;

class Frontend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $langList = ['en', 'fr'];
        if( in_array(\Request::segment(1), $langList)) 
            app()->setlocale(\Request::segment(1));
        return $next($request);
    }
}
