<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {

    /**
     * Generated
     */

    protected $table = 'language';
    protected $fillable = ['lang_id', 'lang_name', 'lang_code', 'lang_flag', 'lang_sort'];


    public function optionGroups() {
        return $this->hasMany(\App\Models\OptionGroup::class, 'option', 'opt_lang', 'opt_group');
    }

    public function categories() {
        return $this->hasMany(\App\Models\Category::class, 'cat_lang', 'lang_id');
    }

    public function options() {
        return $this->hasMany(\App\Models\Option::class, 'opt_lang', 'lang_id');
    }

    public function posts() {
        return $this->hasMany(\App\Models\Post::class, 'post_lang', 'lang_id');
    }


}
