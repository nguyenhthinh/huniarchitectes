<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    /**
     * Generated
     */

    protected $table = 'tag';
    protected $fillable = ['post_id', 'tag'];


    public function post() {
        return $this->belongsTo(\App\Models\Post::class, 'post_id', 'post_id');
    }


}
