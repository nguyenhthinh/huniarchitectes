<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'page';
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_id', 'created_at'];

    function post(){
        return $this->belongsTo(\App\Models\Post::class, 'post_id', 'post_id');
    }

    function alias(){
        return $this->morphOne('\App\Models\Alias', 'obj');
    }
}
