<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alias extends Model {

    /**
     * Generated
     */

    protected $table = 'alias';
    protected $fillable = ['obj_id', 'obj_type', 'alias'];


    public $timestamps = false;

    public function obj()
    {
        return $this->morphTo();
    }

}
