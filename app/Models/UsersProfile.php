<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersProfile extends Model {

    /**
     * Generated
     */

    protected $table = 'users_profile';
    protected $fillable = ['profile_id', 'user_id', 'firstname', 'lastname', 'dateofbirth', 'phone', 'avatar', 'address'];


    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
