<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {

    /**
     * Generated
     */

    protected $table = 'option';
    protected $primaryKey = 'opt_id';
    protected $fillable = ['opt_id', 'opt_group', 'opt_name', 'opt_value', 'opt_lang'];
    public $timestamps = false;


    public function optionGroup() {
        return $this->belongsTo(\App\Models\OptionGroup::class, 'opt_group', 'opt_g_id');
    }

    public function language() {
        return $this->belongsTo(\App\Models\Language::class, 'opt_lang', 'lang_id');
    }


}
