<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    /**
     * Generated
     */

    protected $table = 'project';
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_id', 'gallery','video', 'year', 'location', 'area', 'cost', 'designer', 'description'];


    public function post() {
        return $this->belongsTo(\App\Models\Post::class, 'post_id', 'post_id');
    }
    public function alias() {
        return $this->morphOne('\App\Models\Alias', 'obj');
    }

}
