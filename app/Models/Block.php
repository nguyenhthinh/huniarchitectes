<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'block';
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_id', 'slug', 'created_at'];


    public function post() {
        return $this->belongsTo(\App\Models\Post::class, 'post_id', 'post_id');
    }
    public function alias() {
        return $this->morphOne('\App\Models\Alias', 'obj');
    }
}
