<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    /**
     * Generated
     */

    protected $table = 'article';
    protected $primaryKey = 'article_id';
    protected $fillable = ['article_id', 'article_meta_title','article_meta_short_description', 'content', 'article_meta_keyword', 'article_lang', 'article_category', 'article_type', 'article_author', 'article_publish', 'article_publish_date'];


    public function language() {
        return $this->belongsTo(\App\Models\Language::class, 'article_lang', 'lang_id');
    }

    public function category() {
        return $this->belongsTo(\App\Models\Category::class, 'article_category', 'cat_id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'article_author', 'id');
    }

    public function tags() {
        return $this->hasMany(\App\Models\Tag::class, 'article_id', 'article_id');
    }

    public function alias() {
        return $this->morphOne('\App\Models\Alias', 'obj');
    }

    public function news(){
        return $this->hasOne(\App\Models\Project::class, 'article_id', 'article_id');
    }

    public function page(){
        return $this->hasOne(\App\Models\Page::class, 'article_id', 'article_id');
    }

    public function block(){
        return $this->hasOne(\App\Models\Block::class, 'article_id', 'article_id');
    }

}
