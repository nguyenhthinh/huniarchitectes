<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryBlock extends Model
{
    //
    protected $table = 'category_block';
    protected $primaryKey = 'cat_id';

    function category(){
        return $this->belongsTo(\App\Models\Category::class, 'cat_id', 'cat_id');
    }

    public function alias() {
        return $this->morphOne('\App\Models\Alias', 'obj');
    }
}
