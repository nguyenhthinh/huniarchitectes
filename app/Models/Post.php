<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    /**
     * Generated
     */

    protected $table = 'post';
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_id', 'post_name','post_order', 'post_expert', 'post_content', 'post_image', 'post_meta_title', 'post_meta_description', 'post_meta_keyword', 'post_lang', 'post_category', 'post_type', 'post_author', 'post_publish', 'post_publish_date'];


    public function language() {
        return $this->belongsTo(\App\Models\Language::class, 'post_lang', 'lang_id');
    }

    public function category() {
        return $this->belongsTo(\App\Models\Category::class, 'post_category', 'cat_id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'post_author', 'id');
    }

    public function tags() {
        return $this->hasMany(\App\Models\Tag::class, 'post_id', 'post_id');
    }

    public function alias() {
        return $this->morphOne('\App\Models\Alias', 'obj');
    }

    public function project(){
        return $this->hasOne(\App\Models\Project::class, 'post_id', 'post_id');
    }

    public function page(){
        return $this->hasOne(\App\Models\Page::class, 'post_id', 'post_id');
    }

    public function block(){
        return $this->hasOne(\App\Models\Block::class, 'post_id', 'post_id');
    }

}
