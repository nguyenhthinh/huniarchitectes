<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable {

    /**
     * Generated
     */

    use Notifiable;
    use EntrustUserTrait;

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'email', 'email_verified_at', 'password', 'remember_token'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
        return $this->belongsToMany(\App\Models\Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function posts() {
        return $this->hasMany(\App\Models\Post::class, 'post_author', 'id');
    }

    public function roleUsers() {
        return $this->hasMany(\App\Models\RoleUser::class, 'user_id', 'id');
    }

    public function usersProfiles() {
        return $this->hasOne(\App\Models\UsersProfile::class, 'user_id', 'id');
    }


}
