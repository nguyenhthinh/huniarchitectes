<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    /**
     * Generated
     */

    protected $table = 'category';
    protected $primaryKey = 'cat_id';
    protected $fillable = ['cat_id', 'cat_name', 'cat_image', 'cat_order', 'cat_description', 'cat_meta_title', 'cat_meta_description', 'cat_meta_keywords', 'cat_lang', 'cat_parent', 'cat_type'];


    public function language() {
        return $this->belongsTo(\App\Models\Language::class, 'cat_lang', 'lang_id');
    }

    public function posts() {
        return $this->hasMany(\App\Models\Post::class, 'post_category', 'cat_id');
    }

    public function alias() {
        return $this->morphOne('\App\Models\Alias', 'obj');
    }

    public function parent(){
        return $this->belongsTo(\App\Models\Category::class, 'cat_parent', 'cat_id');
    }

    public function project(){
        return $this->hasOne(\App\Models\CategoryProject::class, 'cat_id', 'cat_id');
    }
    public function block(){
        return $this->hasOne(\App\Models\CategoryBlock::class, 'cat_id', 'cat_id');
    }
    public function page(){
        return $this->hasOne(\App\Models\CategoryPage::class, 'cat_id', 'cat_id');
    }
}
